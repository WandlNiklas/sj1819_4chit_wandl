﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TimeTool;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Linq;

namespace TimeToolTest
{
    [TestClass]
    public class TimeToolTestClass
    {
        Matrix TimeTool;
        public TimeToolTestClass()
        {
            if (File.Exists("Aufgaben.xml")){
                File.Delete("Aufgaben.xml"); }
            XDocument xDoc = new XDocument(new XElement("Aufgaben",new XElement("Aufgabe",
                new XElement("Name", "Testaufgabe 1"),
                new XElement("Beschreibung", "Beschreibung der Aufgabe"),
                new XAttribute("wichtig", true),
                new XAttribute("dringend", false),
                new XAttribute("erledigt", true.ToString().ToLower())
                )));

            StringWriter sw = new StringWriter();
            XmlWriter xWrite = XmlWriter.Create(sw);
            xDoc.Save(xWrite);
            xWrite.Close();
            xDoc.Save("Aufgaben.xml");
            TimeTool = new Matrix();
        }
        [TestMethod]
        public void TestAdd()
        {
            TimeTool.AddAufgabe("Testaufgabe 2", "Hi", false, false,false);
            string file1 = File.ReadAllText("Aufgaben.xml");
            string file2 = File.ReadAllText("AufgabenAddCompare.xml");
            Assert.AreEqual(file1, file2);
        }
        [TestMethod]
        public void TestEdit()
        {
            TimeTool.EditAufgabe("Testaufgabe 2", "Testaufgabe Replaced", "Hi", false, false,false);
            string file1 = File.ReadAllText("Aufgaben.xml");
            string file2 = File.ReadAllText("AufgabenEditCompare.xml");
            Assert.AreEqual(file1, file2);
        }
        [TestMethod]
        public void TestShow()
        {
            Aufgabe a1 = TimeTool.ShowAufgabe("Testaufgabe 1");
            IEnumerable<Aufgabe> a2 = from a in XDocument.Load("Aufgaben.xml")
                    .Descendants("Aufgaben").Descendants("Aufgabe").Where(node => (string)node.Element("Name") == "Testaufgabe 1")
            select new Aufgabe
            {
                Name = a.Element("Name").Value,
                Beschreibung = a.Element("Beschreibung").Value,
                Wichtig = Convert.ToBoolean(a.Attribute("wichtig").Value),
                Dringend = Convert.ToBoolean(a.Attribute("dringend").Value),
                Erledigt = Convert.ToBoolean(a.Attribute("erledigt").Value)
            };
            List<Aufgabe> a2l = a2.ToList();
            Assert.AreEqual(a1.Name, a2l[0].Name);
            Assert.AreEqual(a1.Beschreibung, a2l[0].Beschreibung);
            Assert.AreEqual(a1.Wichtig, a2l[0].Wichtig);
            Assert.AreEqual(a1.Dringend, a2l[0].Dringend);
            Assert.AreEqual(a1.Erledigt, a2l[0].Erledigt);
        }
    }
}
