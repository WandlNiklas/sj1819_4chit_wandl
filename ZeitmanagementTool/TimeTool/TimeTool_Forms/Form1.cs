﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TimeTool;

namespace TimeTool_Forms
{
    public partial class Form1 : Form
    {
        List<CheckedListBox> clbs;
        Matrix TimeTool;
        Form fAdd;
        string header="", bes="";
        bool w=false, d=false;
        public Form1()
        {
            InitializeComponent();
            clbs = new List<CheckedListBox>();
            clbs.Add(lbA);
            clbs.Add(lbB);
            clbs.Add(lbC);
            clbs.Add(lbD);
            Setup();
        }
        
        private void Setup()
        {
            TimeTool = new Matrix();

            foreach(CheckedListBox clb in clbs)
            {
                clb.Items.Clear();
            }

            foreach (Aufgabe a in TimeTool.Aufgaben)
            {
                if (a.Wichtig && a.Dringend)
                    lbA.Items.Add(a.Name);
                else if (a.Wichtig && !a.Dringend)
                    lbB.Items.Add(a.Name);
                else if (!a.Wichtig && a.Dringend)
                    lbC.Items.Add(a.Name);
                else
                    lbD.Items.Add(a.Name);
            }
            foreach (CheckedListBox clb in clbs)
            {
                for (int i = 0; i < clb.Items.Count; i++)
                {
                    Aufgabe a1 = null;
                    if (TimeTool.Aufgaben.Exists(a => a.Name == clb.Items[i].ToString()))
                    {
                        a1 = TimeTool.Aufgaben.Where(a => a.Name == clb.Items[i].ToString()).ToList()[0];
                    }
                    if (a1.Erledigt)
                        clb.SetItemChecked(i, true);
                }
                clb.SelectedIndexChanged += Item_Click;
                clb.ItemCheck += Clb_ItemCheck;
            }
        }

        private void Clb_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;
            if (clb.SelectedItem != null)
            {
                if (TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Erledigt == true)
                {
                    TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Erledigt = false;
                    TimeTool.EditAufgabe(TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Beschreibung,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Wichtig,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Dringend,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Erledigt);
                    return;
                }
                if (TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Erledigt == false)
                {
                    TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Erledigt = true;
                    TimeTool.EditAufgabe(TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Beschreibung,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Wichtig,
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Dringend, 
                        TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Erledigt);
                    return;
                }
            }
        }

        private void Item_Click(object sender, EventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;
            if (clb.SelectedItem != null)
            {
                header = clb.SelectedItem.ToString();
                bes = TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Beschreibung.ToString();
                w = TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Wichtig;
                d = TimeTool.Aufgaben.Where(a => a.Name == clb.SelectedItem.ToString()).ToList()[0].Dringend;
                lbHeader.Text = header;
                tbBes.Text = bes;
            }
        }

        private void buttonAusblenden_Click(object sender, EventArgs e)
        {
            if (buttonAusblenden.Text == "Erledigte ausblenden")
            {
                foreach (CheckedListBox clb in clbs)
                {
                    clb.Items.Clear();
                }
                foreach (Aufgabe a in TimeTool.Aufgaben)
                {
                    if (!a.Erledigt)
                    {
                        if (a.Wichtig && a.Dringend)
                            lbA.Items.Add(a.Name);
                        else if (a.Wichtig && !a.Dringend)
                            lbB.Items.Add(a.Name);
                        else if (!a.Wichtig && a.Dringend)
                            lbC.Items.Add(a.Name);
                        else
                            lbD.Items.Add(a.Name);
                    }
                }
                buttonAusblenden.Text = "Erledigte einblenden";
            }
            else
            {
                foreach (Aufgabe a in TimeTool.Aufgaben)
                {
                    if (a.Erledigt)
                    {
                        if (a.Wichtig && a.Dringend)
                            lbA.Items.Add(a.Name);
                        else if (a.Wichtig && !a.Dringend)
                            lbB.Items.Add(a.Name);
                        else if (!a.Wichtig && a.Dringend)
                            lbC.Items.Add(a.Name);
                        else
                            lbD.Items.Add(a.Name);
                    }
                }
                foreach (CheckedListBox clb in clbs)
                {
                    for (int i = 0; i < clb.Items.Count; i++)
                    {
                        if (TimeTool.Aufgaben.Where(a => a.Name == clb.Items[i].ToString()).ToList()[0].Erledigt == true)
                        {
                            clb.SetItemChecked(i, true);
                            Clb_ItemCheck(clb, new ItemCheckEventArgs(i,CheckState.Checked,CheckState.Unchecked));
                        }
                    }
                }
                buttonAusblenden.Text = "Erledigte ausblenden";
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            fAdd = new Form();
            fAdd.Width = 280;
            fAdd.Height = 260;
            fAdd.Text = "Aufgabe hinzufügen";

            Label n = new Label();
            n.Top = 20;
            n.Text = "Name:";
            n.Width = 200;
            n.Left = 30;
            fAdd.Controls.Add(n);

            Label b = new Label();
            b.Top = 80;
            b.Text = "Beschreibung:";
            b.Width = 200;
            b.Left = 30;
            fAdd.Controls.Add(b);

            TextBox nt = new TextBox();
            nt.Top = 50;
            nt.Width = 200;
            nt.Left = 30;
            fAdd.Controls.Add(nt);

            TextBox bt = new TextBox();
            bt.Top = 110;
            bt.Width = 200;
            bt.Left = 30;
            fAdd.Controls.Add(bt);

            CheckBox wt = new CheckBox();
            wt.Text = "Wichtig";
            wt.Top = 140;
            wt.Left = 30;
            wt.Width = 100;
            fAdd.Controls.Add(wt);

            CheckBox dt = new CheckBox();
            dt.Text = "Dringend";
            dt.Top = 140;
            dt.Left = 140;
            dt.Width = 100;
            fAdd.Controls.Add(dt);

            Button sb = new Button();
            sb.Text = "Speichern";
            sb.Top = 180;
            sb.Left = 140;
            sb.Width = 90;
            sb.Click += Sb_Click;
            fAdd.Controls.Add(sb);

            Button cb = new Button();
            cb.Text = "Abbrechen";
            cb.Top = 180;
            cb.Left = 30;
            cb.Width = 90;
            cb.Click += Cb_Click;
            fAdd.Controls.Add(cb);

            fAdd.Show();
        }

        private void Sb_Click(object sender, EventArgs e)
        {
            CheckBox w = (CheckBox)fAdd.Controls[4];
            CheckBox d = (CheckBox)fAdd.Controls[5];
            bool wv = false, dv=false;
            if (w.Checked)
                wv = true;
            if (d.Checked)
                dv = true;

            TimeTool.AddAufgabe(fAdd.Controls[2].Text, fAdd.Controls[3].Text, wv, dv,false);
            fAdd.Close();
            Setup();
        }

        private void Cb_Click(object sender, EventArgs e)
        {
            fAdd.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            fAdd = new Form();
            fAdd.Width = 280;
            fAdd.Height = 260;
            fAdd.Text = "Aufgabe bearbeiten";

            Label n = new Label();
            n.Top = 20;
            n.Text = "Neuer Name:";
            n.Width = 200;
            n.Left = 30;
            fAdd.Controls.Add(n);

            Label b = new Label();
            b.Top = 80;
            b.Text = "Neue Beschreibung:";
            b.Width = 200;
            b.Left = 30;
            fAdd.Controls.Add(b);

            TextBox nt = new TextBox();
            nt.Top = 50;
            nt.Width = 200;
            nt.Left = 30;
            nt.Text = header;
            fAdd.Controls.Add(nt);

            TextBox bt = new TextBox();
            bt.Top = 110;
            bt.Width = 200;
            bt.Left = 30;
            bt.Text = bes;
            fAdd.Controls.Add(bt);

            CheckBox wt = new CheckBox();
            wt.Text = "Wichtig";
            wt.Top = 140;
            wt.Left = 30;
            wt.Width = 100;
            if (w == true)
                wt.CheckState = CheckState.Checked;
            else
                wt.CheckState = CheckState.Unchecked;
            fAdd.Controls.Add(wt);

            CheckBox dt = new CheckBox();
            dt.Text = "Dringend";
            dt.Top = 140;
            dt.Left = 140;
            dt.Width = 100;
            if (d == true)
                dt.CheckState = CheckState.Checked;
            else
                dt.CheckState = CheckState.Unchecked;
            fAdd.Controls.Add(dt);

            Button sb = new Button();
            sb.Text = "Speichern";
            sb.Top = 180;
            sb.Left = 140;
            sb.Width = 90;
            sb.Click += Sbe_Click;
            fAdd.Controls.Add(sb);

            Button cb = new Button();
            cb.Text = "Abbrechen";
            cb.Top = 180;
            cb.Left = 30;
            cb.Width = 90;
            cb.Click += Cb_Click;
            fAdd.Controls.Add(cb);

            fAdd.Show();
        }
        private void Sbe_Click(object sender, EventArgs e)
        {
            CheckBox w = (CheckBox)fAdd.Controls[4];
            CheckBox d = (CheckBox)fAdd.Controls[5];
            bool wv = false, dv = false;
            if (w.Checked)
                wv = true;
            if (d.Checked)
                dv = true;

            TimeTool.EditAufgabe(header,fAdd.Controls[2].Text, fAdd.Controls[3].Text, wv, dv, TimeTool.Aufgaben.Where(a => a.Name == header).ToList()[0].Erledigt);
            fAdd.Close();
            Setup();
        }
    }
}
