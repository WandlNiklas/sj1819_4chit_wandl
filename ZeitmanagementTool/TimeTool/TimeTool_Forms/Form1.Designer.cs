﻿namespace TimeTool_Forms
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbA = new System.Windows.Forms.CheckedListBox();
            this.lbB = new System.Windows.Forms.CheckedListBox();
            this.lbC = new System.Windows.Forms.CheckedListBox();
            this.lbD = new System.Windows.Forms.CheckedListBox();
            this.labelA = new System.Windows.Forms.Label();
            this.labelB = new System.Windows.Forms.Label();
            this.labelD = new System.Windows.Forms.Label();
            this.labelC = new System.Windows.Forms.Label();
            this.buttonAusblenden = new System.Windows.Forms.Button();
            this.tbBes = new System.Windows.Forms.TextBox();
            this.lbHeader = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbA
            // 
            this.lbA.FormattingEnabled = true;
            this.lbA.Location = new System.Drawing.Point(335, 37);
            this.lbA.Name = "lbA";
            this.lbA.Size = new System.Drawing.Size(227, 169);
            this.lbA.TabIndex = 1;
            // 
            // lbB
            // 
            this.lbB.FormattingEnabled = true;
            this.lbB.Location = new System.Drawing.Point(12, 37);
            this.lbB.Name = "lbB";
            this.lbB.Size = new System.Drawing.Size(231, 169);
            this.lbB.TabIndex = 2;
            // 
            // lbC
            // 
            this.lbC.FormattingEnabled = true;
            this.lbC.Location = new System.Drawing.Point(335, 249);
            this.lbC.Name = "lbC";
            this.lbC.Size = new System.Drawing.Size(227, 169);
            this.lbC.TabIndex = 3;
            // 
            // lbD
            // 
            this.lbD.FormattingEnabled = true;
            this.lbD.Location = new System.Drawing.Point(12, 249);
            this.lbD.Name = "lbD";
            this.lbD.Size = new System.Drawing.Size(231, 169);
            this.lbD.TabIndex = 4;
            // 
            // labelA
            // 
            this.labelA.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelA.Location = new System.Drawing.Point(331, 10);
            this.labelA.Name = "labelA";
            this.labelA.Size = new System.Drawing.Size(178, 24);
            this.labelA.TabIndex = 5;
            this.labelA.Text = "Wichtig + dringend:";
            // 
            // labelB
            // 
            this.labelB.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelB.Location = new System.Drawing.Point(12, 10);
            this.labelB.Name = "labelB";
            this.labelB.Size = new System.Drawing.Size(231, 24);
            this.labelB.TabIndex = 6;
            this.labelB.Text = "Wichtig + nicht dringend:";
            // 
            // labelD
            // 
            this.labelD.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD.Location = new System.Drawing.Point(12, 222);
            this.labelD.Name = "labelD";
            this.labelD.Size = new System.Drawing.Size(267, 24);
            this.labelD.TabIndex = 7;
            this.labelD.Text = "Nicht wichtig + nicht dringend:";
            // 
            // labelC
            // 
            this.labelC.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelC.Location = new System.Drawing.Point(331, 222);
            this.labelC.Name = "labelC";
            this.labelC.Size = new System.Drawing.Size(231, 24);
            this.labelC.TabIndex = 8;
            this.labelC.Text = "Nicht wichtig + dringend:";
            // 
            // buttonAusblenden
            // 
            this.buttonAusblenden.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAusblenden.Location = new System.Drawing.Point(588, 369);
            this.buttonAusblenden.Name = "buttonAusblenden";
            this.buttonAusblenden.Size = new System.Drawing.Size(200, 49);
            this.buttonAusblenden.TabIndex = 9;
            this.buttonAusblenden.Text = "Erledigte ausblenden";
            this.buttonAusblenden.UseVisualStyleBackColor = true;
            this.buttonAusblenden.Click += new System.EventHandler(this.buttonAusblenden_Click);
            // 
            // tbBes
            // 
            this.tbBes.AllowDrop = true;
            this.tbBes.Location = new System.Drawing.Point(588, 54);
            this.tbBes.MaximumSize = new System.Drawing.Size(200, 150);
            this.tbBes.MinimumSize = new System.Drawing.Size(200, 150);
            this.tbBes.Name = "tbBes";
            this.tbBes.ReadOnly = true;
            this.tbBes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbBes.Size = new System.Drawing.Size(200, 150);
            this.tbBes.TabIndex = 10;
            // 
            // lbHeader
            // 
            this.lbHeader.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeader.Location = new System.Drawing.Point(584, 10);
            this.lbHeader.Name = "lbHeader";
            this.lbHeader.Size = new System.Drawing.Size(178, 24);
            this.lbHeader.TabIndex = 11;
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Location = new System.Drawing.Point(588, 304);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(200, 49);
            this.btnAdd.TabIndex = 12;
            this.btnAdd.Text = "Aufgabe hinzufügen";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEdit.Location = new System.Drawing.Point(588, 249);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(200, 49);
            this.btnEdit.TabIndex = 13;
            this.btnEdit.Text = "Aufgabe bearbeiten";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 443);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lbHeader);
            this.Controls.Add(this.tbBes);
            this.Controls.Add(this.buttonAusblenden);
            this.Controls.Add(this.labelC);
            this.Controls.Add(this.labelD);
            this.Controls.Add(this.labelB);
            this.Controls.Add(this.labelA);
            this.Controls.Add(this.lbD);
            this.Controls.Add(this.lbC);
            this.Controls.Add(this.lbB);
            this.Controls.Add(this.lbA);
            this.Name = "Form1";
            this.Text = "TimeTool V1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckedListBox lbA;
        private System.Windows.Forms.CheckedListBox lbB;
        private System.Windows.Forms.CheckedListBox lbC;
        private System.Windows.Forms.CheckedListBox lbD;
        private System.Windows.Forms.Label labelA;
        private System.Windows.Forms.Label labelB;
        private System.Windows.Forms.Label labelD;
        private System.Windows.Forms.Label labelC;
        private System.Windows.Forms.Button buttonAusblenden;
        private System.Windows.Forms.TextBox tbBes;
        private System.Windows.Forms.Label lbHeader;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnEdit;
    }
}

