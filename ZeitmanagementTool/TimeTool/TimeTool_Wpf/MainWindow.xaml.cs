﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TimeTool;

namespace TimeTool_Wpf
{
    public partial class MainWindow : Window
    {
        Window fAdd;
        TimeTool.Matrix TimeTool;
        List<ListBox> lbl;
        string header = "", bes = "";
        bool w = false, d = false;
        public MainWindow()
        {
            InitializeComponent();
            lbl = new List<ListBox>();
            lbl.Add(wichtigdringend);
            lbl.Add(wichtignichtdringend);
            lbl.Add(nichtwichtigdringend);
            lbl.Add(nichtwichtignichtdringend);
            Setup();
        }
        private void Setup()
        {
            TimeTool = new TimeTool.Matrix();

            foreach (ListBox lb in lbl)
            {
                lb.Items.Clear();
            }

            foreach (Aufgabe a in TimeTool.Aufgaben)
            {
                CheckBox cb = new CheckBox { Content = a.Name, Width=wichtigdringend.Width-20};
                if (a.Wichtig && a.Dringend)
                    wichtigdringend.Items.Add(cb);
                else if (a.Wichtig && !a.Dringend)
                    wichtignichtdringend.Items.Add(cb);
                else if (!a.Wichtig && a.Dringend)
                    nichtwichtigdringend.Items.Add(cb);
                else
                    nichtwichtignichtdringend.Items.Add(cb);
            }
            foreach (ListBox lb in lbl)
            {
                foreach(CheckBox cb in lb.Items)
                { 
                    Aufgabe a1 = null;
                    if (TimeTool.Aufgaben.Exists(a => a.Name == cb.Content.ToString()))
                    {
                        a1 = TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0];

                        if (a1.Erledigt)
                            cb.IsChecked = true;
                    }
                    cb.Checked += Cb_Checked;
                    cb.Unchecked += Cb_Checked;
                    cb.Click += Cb_Click;
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Cb_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb != null)
            {
                header = cb.Content.ToString();
                bes = TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Beschreibung.ToString();
                w = TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Wichtig;
                d = TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Dringend;
                lbHeader.Content = header;
                tbBes.Text = bes;
            }
        }

        private void Cb_Checked(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            if (cb != null)
            {
                if (TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Erledigt == true)
                {
                    TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Erledigt = false;
                    TimeTool.EditAufgabe(TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Beschreibung,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Wichtig,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Dringend,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Erledigt);
                    return;
                }
                if (TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Erledigt == false)
                {
                    TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Erledigt = true;
                    TimeTool.EditAufgabe(TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Name,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Beschreibung,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Wichtig,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Dringend,
                        TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Erledigt);
                    return;
                }
            }
        }
        private void buttonAusblenden_Click(object sender, EventArgs e)
        {
            if ((string)buttonAusblenden.Content == "Erledigte ausblenden")
            {
                foreach (ListBox lb in lbl)
                {
                    lb.Items.Clear();
                }
                foreach (Aufgabe a in TimeTool.Aufgaben)
                {
                    if (!a.Erledigt)
                    {
                        CheckBox cb = new CheckBox { Content = a.Name, Width = wichtigdringend.Width - 20 };
                        if (a.Wichtig && a.Dringend)
                            wichtigdringend.Items.Add(cb);
                        else if (a.Wichtig && !a.Dringend)
                            wichtignichtdringend.Items.Add(cb);
                        else if (!a.Wichtig && a.Dringend)
                            nichtwichtigdringend.Items.Add(cb);
                        else
                            nichtwichtignichtdringend.Items.Add(cb);
                        cb.Checked += Cb_Checked;
                        cb.Unchecked += Cb_Checked;
                        cb.Click += Cb_Click;
                    }
                }
                buttonAusblenden.Content = "Erledigte einblenden";
            }
            else
            {
                foreach (Aufgabe a in TimeTool.Aufgaben)
                {
                    if (a.Erledigt)
                    {
                        CheckBox cb = new CheckBox { Content = a.Name, Width = wichtigdringend.Width - 20 };
                        if (a.Wichtig && a.Dringend)
                            wichtigdringend.Items.Add(cb);
                        else if (a.Wichtig && !a.Dringend)
                            wichtignichtdringend.Items.Add(cb);
                        else if (!a.Wichtig && a.Dringend)
                            nichtwichtigdringend.Items.Add(cb);
                        else
                            nichtwichtignichtdringend.Items.Add(cb);
                        cb.Checked += Cb_Checked;
                        cb.Unchecked += Cb_Checked;
                        cb.Click += Cb_Click;
                    }
                }
                foreach (ListBox lb in lbl)
                {
                    foreach (CheckBox cb in lb.Items)
                    {
                        if (TimeTool.Aufgaben.Where(a => a.Name == cb.Content.ToString()).ToList()[0].Erledigt == true)
                        {
                            cb.IsChecked = true;
                            Cb_Checked(cb, new RoutedEventArgs());
                        }
                    }
                }
                buttonAusblenden.Content = "Erledigte ausblenden";
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            fAdd = new Window();
            fAdd.Width = 320;
            fAdd.Height = 320;
            fAdd.Title = "Aufgabe hinzufügen";

            Grid g = new Grid();
            g.Height = fAdd.Height;
            g.Width = fAdd.Width;

            
            Label n = new Label();
            n.Margin = new Thickness(15,20,0,0);
            n.Content = "Name:";
            n.Width = 200;
            g.Children.Add(n);

            Label b = new Label();
            b.Margin = new Thickness(15, 80, 0, 0);
            b.Content = "Beschreibung:";
            b.Width = 200;
            g.Children.Add(b);

            TextBox nt = new TextBox();
            nt.Margin = new Thickness(15, -190, 0, 0);
            nt.Width = 200;
            nt.Height = 30;
            g.Children.Add(nt);

            TextBox bt = new TextBox();
            bt.Margin = new Thickness(15, -80, 0, 0);
            bt.Width = 200;
            bt.Height = 30;
            g.Children.Add(bt);

            CheckBox wt = new CheckBox();
            wt.Content = "Wichtig";
            wt.Margin = new Thickness(-80, 150, 0, 0);
            wt.Width = 100;
            g.Children.Add(wt);

            CheckBox dt = new CheckBox();
            dt.Content = "Dringend";
            dt.Margin = new Thickness(140, 150, 0, 0);
            dt.Width = 100;
            g.Children.Add(dt);

            Button sb = new Button();
            sb.Content = "Speichern";
            sb.Margin = new Thickness(120, 90, 0, 0);
            sb.Width = 90;
            sb.Height = 30;
            sb.Click += Saveb_Click;
            g.Children.Add(sb);

            Button cb = new Button();
            cb.Content = "Abbrechen";
            cb.Margin = new Thickness(-100, 90, 0, 0);
            cb.Width = 90;
            cb.Height = 30;
            cb.Click += Cancelb_Click;
            g.Children.Add(cb);

            fAdd.Content = g;
            fAdd.Show();
        }

        private void Saveb_Click(object sender, EventArgs e)
        {
            Grid g = (Grid)fAdd.Content;
            CheckBox w = (CheckBox)g.Children[4];
            CheckBox d = (CheckBox)g.Children[5];
            bool wv = false, dv = false;
            if ((bool)w.IsChecked)
                wv = true;
            if ((bool)d.IsChecked)
                dv = true;

            TextBox name = (TextBox)g.Children[2];
            TextBox bes = (TextBox)g.Children[3];

            TimeTool.AddAufgabe(name.Text, bes.Text, wv, dv, false);
            fAdd.Close();
            Setup();
        }

        private void Cancelb_Click(object sender, EventArgs e)
        {
            fAdd.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            fAdd = new Window();
            fAdd.Width = 320;
            fAdd.Height = 320;
            fAdd.Title = "Aufgabe bearbeiten";

            Grid g = new Grid();
            g.Height = fAdd.Height;
            g.Width = fAdd.Width;


            Label n = new Label();
            n.Margin = new Thickness(15, 20, 0, 0);
            n.Content = "Neuer Name:";
            n.Width = 200;
            g.Children.Add(n);

            Label b = new Label();
            b.Margin = new Thickness(15, 80, 0, 0);
            b.Content = "Neue Beschreibung:";
            b.Width = 200;
            g.Children.Add(b);

            TextBox nt = new TextBox();
            nt.Margin = new Thickness(15, -190, 0, 0);
            nt.Width = 200;
            nt.Height = 30;
            nt.Text = header;
            g.Children.Add(nt);

            TextBox bt = new TextBox();
            bt.Margin = new Thickness(15, -80, 0, 0);
            bt.Width = 200;
            bt.Height = 30;
            bt.Text = bes;
            g.Children.Add(bt);

            CheckBox wt = new CheckBox();
            wt.Content = "Wichtig";
            wt.Margin = new Thickness(-80, 150, 0, 0);
            wt.Width = 100;
            if (w == true)
                wt.IsChecked = true;
            else
                wt.IsChecked = false;
            g.Children.Add(wt);

            CheckBox dt = new CheckBox();
            dt.Content = "Dringend";
            dt.Margin = new Thickness(140, 150, 0, 0);
            dt.Width = 100;
            if (d == true)
                dt.IsChecked = true;
            else
                dt.IsChecked = false;
            g.Children.Add(dt);

            Button sb = new Button();
            sb.Content = "Speichern";
            sb.Margin = new Thickness(120, 90, 0, 0);
            sb.Width = 90;
            sb.Height = 30;
            sb.Click += Savebedit_Click;
            g.Children.Add(sb);

            Button cb = new Button();
            cb.Content = "Abbrechen";
            cb.Margin = new Thickness(-100, 90, 0, 0);
            cb.Width = 90;
            cb.Height = 30;
            cb.Click += Cancelb_Click;
            g.Children.Add(cb);

            fAdd.Content = g;
            fAdd.Show();
        }
        private void Savebedit_Click(object sender, EventArgs e)
        {
            Grid g = (Grid)fAdd.Content;
            CheckBox w = (CheckBox)g.Children[4];
            CheckBox d = (CheckBox)g.Children[5];
            bool wv = false, dv = false;
            if ((bool)w.IsChecked)
                wv = true;
            if ((bool)d.IsChecked)
                dv = true;

            TextBox name = (TextBox)g.Children[2];
            TextBox bes = (TextBox)g.Children[3];

            TimeTool.EditAufgabe(header, name.Text, bes.Text, wv, dv, TimeTool.Aufgaben.Where(a => a.Name == header).ToList()[0].Erledigt);
            fAdd.Close();
            Setup();
        }
    }
}
