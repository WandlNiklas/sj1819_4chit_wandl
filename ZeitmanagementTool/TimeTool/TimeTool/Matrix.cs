﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Xml;
using System.Xml.Linq;

namespace TimeTool
{
    public class Matrix
    {
        public List<Aufgabe> Aufgaben = new List<Aufgabe>();

        public Matrix()
        {
            Aufgaben = CreateCustomers().ToList();
        }
        static IEnumerable<Aufgabe> CreateCustomers()
        {
            return
                from a in XDocument.Load("Aufgaben.xml")
                    .Descendants("Aufgaben").Descendants("Aufgabe")
                select new Aufgabe
                {
                    Name = a.Element("Name").Value,
                    Beschreibung = a.Element("Beschreibung").Value,
                    Wichtig = Convert.ToBoolean(a.Attribute("wichtig").Value),
                    Dringend = Convert.ToBoolean(a.Attribute("dringend").Value),
                    Erledigt = Convert.ToBoolean(a.Attribute("erledigt").Value)
                };
        }
        public void AddAufgabe(string name, string beschreibung, bool w, bool d, bool e)
        {
            XDocument xDoc = XDocument.Load("Aufgaben.xml");

            var lastaufgabe = xDoc.Descendants("Aufgaben").FirstOrDefault();
            lastaufgabe.Add(new XElement("Aufgabe",
                new XElement("Name", name),
                new XElement("Beschreibung", beschreibung),
                new XAttribute("wichtig", w),
                new XAttribute("dringend",d),
                new XAttribute("erledigt", e)
                ));

            StringWriter sw = new StringWriter();
            XmlWriter xWrite = XmlWriter.Create(sw);
            xDoc.Save(xWrite);
            xWrite.Close();
            xDoc.Save("Aufgaben.xml");
        }

        public void EditAufgabe(string name, string newname, string newbeschreibung, bool neww, bool newd, bool newe)
        {
            XDocument xDoc = XDocument.Load("Aufgaben.xml");

            var aufgabe = xDoc.Descendants("Aufgaben").Elements("Aufgabe").Where(node => (string)node.Element("Name") == name);
            //if (aufgabe.LastOrDefault() !=null)
            //{
                aufgabe.Remove();
                StringWriter sw = new StringWriter();
                XmlWriter xWrite = XmlWriter.Create(sw);
                xDoc.Save(xWrite);
                xWrite.Close();
                xDoc.Save("Aufgaben.xml");

                AddAufgabe(newname, newbeschreibung, neww, newd, newe);
            //}
        }

        public Aufgabe ShowAufgabe(string name)
        {
            var aufgaben = Aufgaben.Where(a => a.Name == name);
            return aufgaben.ToList()[0];
        }
    }
}