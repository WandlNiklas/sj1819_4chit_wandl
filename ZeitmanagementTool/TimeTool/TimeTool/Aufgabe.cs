﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TimeTool
{
    public class Aufgabe
    {
        public string Name { get; set; }

        public string Beschreibung { get; set; }

        public bool Erledigt { get; set; }

        public bool Wichtig { get; set; }

        public bool Dringend { get; set; }
        
    }
}