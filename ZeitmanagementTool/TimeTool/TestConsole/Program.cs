﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TimeTool;

namespace TestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix m = new Matrix();
            //m.AddAufgabe("Testaufgabe 6", "ajkölddjföalkf", false, false);
            m.EditAufgabe("Testaufgabe 2", "Testaufgabe Replaced", "aödföadf", false, false,false);
            Aufgabe a = m.ShowAufgabe("Testaufgabe 1");
            Console.WriteLine(a.Name+a.Beschreibung+a.Wichtig+a.Dringend+a.Erledigt);
        }
    }
}
