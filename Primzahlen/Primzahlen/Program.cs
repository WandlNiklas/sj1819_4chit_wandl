﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace Primzahlen
{
    public class Program
    {
        static void Main(string[] args)
        {
            Prim_Calculation pc = new Prim_Calculation(100000);
            Console.WriteLine("Test 6)"); //Higher Boundary
            Thread four = new Thread(() => pc.CalculatePrims(100));
            four.Start();
            four.Join();

            Console.WriteLine("Test 8/1)"); //Lower and Higher Boundaries: Array
            string[] boundaries = new string[2];
            boundaries[0] = "100"; boundaries[1] = "200";
            Thread eightone = new Thread(pc.CalculatePrimsBoundariesArray);
            eightone.Start(boundaries);
            eightone.Join();

            Console.WriteLine("Test 8/2)"); //Lower and Higher Boundaries: Wrapper
            Wrapper w = new Wrapper(100, 200);
            Thread eighttwo = new Thread(pc.CalculatePrimsBoundariesWrapper);
            eighttwo.Start(w);
            eighttwo.Join();

            Console.WriteLine("Test 8/3)"); //Lower and Higher Boundaries: Lambda
            Thread eightthree = new Thread(()=>pc.CalculatePrimsBoundariesLambda(100,200));
            eightthree.Start();
            eightthree.Join();

            Console.WriteLine("Test 9)");
            Thread t = new Thread(() => pc.CalculatePrims(100000));
            t.IsBackground = true;
            t.Name = "CalculatePrimsInBackgroundThread";
            t.Start();
            Stopwatch s = Stopwatch.StartNew();
            t.Join();
            s.Stop();
            Console.WriteLine(t.Name + " has finished, it took " + s.ElapsedMilliseconds + " milliseconds and its status is: " + t.ThreadState);

            Thread t2 = new Thread(() => pc.CalculatePrims(100000));
            t2.IsBackground = true;
            t2.Name = "CalculatePrimsInBackgroundThread2";
            t2.Start();
            Stopwatch s2 = Stopwatch.StartNew();
            t2.Join();
            s2.Stop();
            Console.WriteLine(t2.Name + " has finished, it took " + s2.ElapsedMilliseconds + " milliseconds and its status is: " + t2.ThreadState);

            Thread t3 = new Thread(() => pc.CalculatePrims(100000));
            t3.IsBackground = true;
            t3.Name = "CalculatePrimsInBackgroundThread3";
            t3.Start();
            Stopwatch s3 = Stopwatch.StartNew();
            t3.Join();
            s3.Stop();
            Console.WriteLine(t3.Name + " has finished, it took " + s3.ElapsedMilliseconds + " milliseconds and its status is: " + t3.ThreadState);

            Console.WriteLine("Test 10)");
            Thread t4 = new Thread(() => pc.CalculatePrimsOneArray());
            t4.IsBackground = true;
            t4.Name = "CalculatePrimsInBackgroundThread4";
            t4.Start();
            Stopwatch s4 = Stopwatch.StartNew();
            t4.Join();
            s4.Stop();
            Console.WriteLine(t4.Name+ " has finished, it took " + s4.ElapsedMilliseconds + " milliseconds and its status is: " + t4.ThreadState);

            Thread t5 = new Thread(() => pc.CalculatePrimsOneArray());
            t5.IsBackground = true;
            t5.Name = "CalculatePrimsInBackgroundThread5";
            t5.Start();
            Stopwatch s5 = Stopwatch.StartNew();
            t5.Join();
            s5.Stop();
            Console.WriteLine(t5.Name + " has finished, it took " + s5.ElapsedMilliseconds + " milliseconds and its status is: " + t5.ThreadState);

            Thread t6 = new Thread(()=>pc.CalculatePrimsOneArray());
            t6.IsBackground = true;
            t6.Name = "CalculatePrimsInBackgroundThread6";
            t6.Start();
            Stopwatch s6 = Stopwatch.StartNew();
            t6.Join();
            s6.Stop();
            Console.WriteLine(t6.Name + " has finished, it took "+s6.ElapsedMilliseconds+" milliseconds and its status is: " + t6.ThreadState);
        }
    }
}
