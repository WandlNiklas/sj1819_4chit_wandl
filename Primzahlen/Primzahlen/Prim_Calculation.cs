﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Primzahlen
{
    public class Prim_Calculation
    {
        bool[] prime_;
        int i;

        public Prim_Calculation(int i_)
        {
            this.i = i_;
            prime_ = new bool[i];
        }
        public void CalculatePrims(object value)
        {
            int n = (int)value;
            bool[] prime = new bool[n];
            for (int i = 2; i < n; i++)
                prime[i] = true;
            {
                int i = 2;
                for (; i * i < n; i++)
                    if (prime[i])
                    {
                        Console.Write(i+";");
                        for (int j = i * i; j < n; j = j + i)
                            prime[j] = false;
                    }
                for (; i < n; i++)
                    if (prime[i])
                    {
                        Console.Write(i + ";");
                    }
            }
            Console.WriteLine();
        }
        public void CalculatePrimsOneArray()
        {
            int n = prime_.Length;
            for (int i = 2; i < n; i++)
                prime_[i] = true;
            {
                int i = 2;
                for (; i * i < n; i++)
                    if (prime_[i])
                    {
                        Console.Write(i + ";");
                        for (int j = i * i; j < n; j = j + i)
                            prime_[j] = false;
                    }
                for (; i < n; i++)
                    if (prime_[i])
                    {
                        Console.Write(i + ";");
                    }
            }
            Console.WriteLine();
        }

        public void CalculatePrimsBoundariesArray(object value)
        {
            string[] v = (string[])value;
            int m = Convert.ToInt32((string)v[0]);
            int n = Convert.ToInt32((string)v[1]);
            bool[] prime = new bool[n];
            for (int i = 2; i < n; i++)
                prime[i] = true;
            {
                int i = 2;
                for (; i * i < n; i++)
                    if (prime[i])
                    {
                        if (i >= m)
                            Console.Write(i + ";");
                        for (int j = i * i; j < n; j = j + i)
                            prime[j] = false;
                    }
                for (; i < n; i++)
                    if (prime[i])
                    {
                        if (i >= m)
                            Console.Write(i + ";");
                    }
            }
            Console.WriteLine();
        }
        public void CalculatePrimsBoundariesWrapper(object value)
        {
            Wrapper w = (Wrapper)value;
            int m = w.Start;
            int n = w.End;
            bool[] prime = new bool[n];
            for (int i = 2; i < n; i++)
                prime[i] = true;
            {
                int i = 2;
                for (; i * i < n; i++)
                    if (prime[i])
                    {
                        if (i >= m)
                            Console.Write(i + ";");
                        for (int j = i * i; j < n; j = j + i)
                            prime[j] = false;
                    }
                for (; i < n; i++)
                    if (prime[i])
                    {
                        if (i >= m)
                            Console.Write(i + ";");
                    }
            }
            Console.WriteLine();
        }
        public void CalculatePrimsBoundariesLambda(int start, int end)
        {
            int m = start;
            int n = end;
            bool[] prime = new bool[n];
            for (int i = 2; i < n; i++)
                prime[i] = true;
            {
                int i = 2;
                for (; i * i < n; i++)
                    if (prime[i])
                    {
                        if (i >= m)
                            Console.Write(i + ";");
                        for (int j = i * i; j < n; j = j + i)
                            prime[j] = false;
                    }
                for (; i < n; i++)
                    if (prime[i])
                    {
                        if (i >= m)
                            Console.Write(i + ";");
                    }
            }
            Console.WriteLine();
        }
        public string CalculatePrimsTest(object value)
        {
            StringBuilder sb = new StringBuilder();
            int n = (int)value;
            bool[] prime = new bool[n];
            for (int i = 2; i < n; i++)
                prime[i] = true;
            {
                int i = 2;
                for (; i * i < n; i++)
                    if (prime[i])
                    {
                        sb.Append(i + ";");
                        for (int j = i * i; j < n; j = j + i)
                            prime[j] = false;
                    }
                for (; i < n; i++)
                    if (prime[i])
                    {
                        sb.Append(i + ";");
                    }
            }
            return sb.ToString();
        }
    }
}
