﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Primzahlen;

namespace Primzahlen_Tests
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestAlgorithmClass()
        {
            Prim_Calculation pc = new Prim_Calculation(100);
            string expected = "2;3;5;7;11;13;17;19;23;29;31;37;41;43;47;53;59;61;67;71;73;79;83;89;97;";

            string result = pc.CalculatePrimsTest(100);

            Assert.AreEqual(expected, result);
        }
    }
}
