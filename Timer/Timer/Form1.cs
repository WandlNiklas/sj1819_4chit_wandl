﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.IO;

namespace Timer
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            timer1.Start();
            timer1.Interval = 500;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Random rnd = new Random();
            Color randomColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
            panel1.BackColor = randomColor;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Thread t = new Thread(Ggt);
            t.IsBackground = true;
            t.Start();
            t.Join();
            Application.Exit();
        }

        private void Ggt()
        {
            double value1 = Convert.ToDouble(textBox1.Text);
            double value2 = Convert.ToDouble(textBox2.Text);
            double help;

            while (value2 != 0)
            {
                help = value1 % value2;
                value1 = value2;
                value2 = help;
            }
            StreamWriter sw = new StreamWriter("results.txt",true);
            sw.Write(value1 + ";");
            sw.Flush();
            sw.Close();
            Thread.Sleep(5000);
            MessageBox.Show(value1.ToString());
        }
    }
}
