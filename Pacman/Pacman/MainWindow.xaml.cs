﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace Pacman
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Image i;
        public MainWindow()
        {
            InitializeComponent();
            i = new Image();
            i.Loaded += I_Loaded;
            mainCanvas.Children.Add(i);
        }

        private void I_Loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(@"C:\Users\Niklas\Desktop\Schule\JAHR 4\SEW\Projekte\190109\Pacman\Pacman\bin\Debug\pacmananimated.gif");
            bi.EndInit();
            Image i = sender as Image;
            i.Source = bi;
            ImageBehavior.SetAnimatedSource(i, bi);
            double left = (mainCanvas.ActualWidth - bi.Width) / 2;
            Canvas.SetLeft(i, left);
            double top = (mainCanvas.ActualHeight - bi.Height) / 2;
            Canvas.SetTop(i, top);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                TransformGroup tg = new TransformGroup();
                RotateTransform rt = new RotateTransform(180);
                TranslateTransform tt = new TranslateTransform(-50, 0);
                ScaleTransform st = new ScaleTransform(1, -1);
                tg.Children.Add(rt);
                tg.Children.Add(tt);
                tg.Children.Add(st);
                i.RenderTransformOrigin = new Point(0.5, 0.5);
                i.RenderTransform = tg;
            }
            if (e.Key == Key.Up)
            {
                TransformGroup tg = new TransformGroup();
                RotateTransform rt = new RotateTransform(270);
                TranslateTransform tt = new TranslateTransform(0,-50);
                tg.Children.Add(rt);
                tg.Children.Add(tt);
                i.RenderTransformOrigin = new Point(0.5, 0.5);
                i.RenderTransform = tg;
            }
            if (e.Key == Key.Right)
            {
                TransformGroup tg = new TransformGroup();
                RotateTransform rt = new RotateTransform(0);
                TranslateTransform tt = new TranslateTransform(50, 0);
                tg.Children.Add(rt);
                tg.Children.Add(tt);
                i.RenderTransformOrigin = new Point(0.5, 0.5);
                i.RenderTransform = tg;
            }
            if (e.Key == Key.Down)
            {
                TransformGroup tg = new TransformGroup();
                RotateTransform rt = new RotateTransform(90);
                TranslateTransform tt = new TranslateTransform(0, 50);
                tg.Children.Add(rt);
                tg.Children.Add(tt);
                i.RenderTransformOrigin = new Point(0.5, 0.5);
                i.RenderTransform = tg;
            }
            if(e.Key == Key.Space)
            {
                i.RenderTransform = new RotateTransform(0);
                i.RenderTransformOrigin = new Point(0.5, 0.5);
            }
        }
    }
} 


