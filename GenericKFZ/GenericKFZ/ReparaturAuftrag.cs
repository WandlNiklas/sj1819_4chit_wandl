﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericKFZ
{
    class ReparaturAuftrag<T, U>
        where T : Auto
        where U : IArtikel
    {
        public string vn {get; set;}
        public string nn { get; set; }
        public Random randy { get; set; }
        public int number { get; set; }
        public T Hersteller;
        public List<U> Artikel = new List<U>();
        private static int count = 0;

        public ReparaturAuftrag(string vorname, string nachname, T hersteller)
        {
            Hersteller = hersteller;
            vn = vorname;
            nn = nachname;
            randy = new Random();
            number = randy.Next(count, 100);
            count++;
        }

        public void ArtikelAdd(U artikel)
        {
            Artikel.Add(artikel);
            artikel.nachBestellen();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            double gespreis=0;
            foreach (U u in Artikel)
            {
                sb.Append(u + "\n");
                gespreis += u.GesPreis();
            }
            return "Reparaturauftrag "+number.ToString()+" für " + vn + " " + nn + "\n\n" + Hersteller + "\n\nArtikelliste:\n" + sb + "\nGesamtpreis:" + gespreis;
        }
    }
}
