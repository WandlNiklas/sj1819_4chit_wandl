﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericKFZ
{
    class Motorteile:IArtikel
    {
        public int Menge { get; set; }
        public double Preis { get; set; }
        public string Bezeichnung { get; set; }
        public double GesPreis_ { get; set; }

        public Motorteile(string bezeichnung,int menge, double preis)
        {
            Bezeichnung = bezeichnung;
            Menge = menge;
            Preis = preis;
        }

        public double GesPreis()
        {
            GesPreis_ = Menge * Preis;
            return GesPreis_;
        }

        public override string ToString()
        {
            return "Stk: " + Menge + " " + Bezeichnung + " - " + GesPreis() + " Euro";
        }

        public void nachBestellen()
        {
            Console.WriteLine("Nachbestellt!");
        }
    }
}
