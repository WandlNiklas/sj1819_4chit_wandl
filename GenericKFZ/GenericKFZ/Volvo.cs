﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericKFZ
{
    class Volvo:Auto
    {
        public Volvo()
        {
            Name = "Volvo";
        }
        public override string ToString()
        {
            return Name + " - " + Autotyp;
        }
    }
}
