﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericKFZ
{
    interface IArtikel
    {
        void nachBestellen();
        double GesPreis();
    }
}
