﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericKFZ
{
    class Program
    {
        static void Main(string[] args)
        {
            Volvo v = new Volvo();
            v.Autotyp = "V40";
            ReparaturAuftrag<Volvo,Motorteile> ra = new ReparaturAuftrag<Volvo, Motorteile>("Hans", "Meyer", v);
            ra.ArtikelAdd(new Motorteile("Oese", 2, 120));
            ra.ArtikelAdd(new Motorteile("Kolben", 1, 10));
            Console.WriteLine(ra);
        }
    }
}
