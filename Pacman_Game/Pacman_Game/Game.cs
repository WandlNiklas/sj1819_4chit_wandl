﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Pacman_Game
{
    public class Game
    {
        public Canvas C;
        int Length;
        public Pacman p;
        public int Rows { get; set; }
        public int Cols { get; set; }
        public int Points = 0;
        List<Pill> pilllist = new List<Pill>();
        public Game(int length, Canvas c)
        {
            C = c;
            //C.Background = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));

            Length = length;
            SetCanvas();
            p = new Pacman(Length / 4, Length, C);
            p.crossed += Pacman_crossed;
        }

        public void SetCanvas()
        {
            Rows = Convert.ToInt32(C.ActualHeight) / Length - 2;
            Cols = Convert.ToInt32(C.ActualWidth) / Length - 2;
            C.Height = (Rows + 2) * Length;
            C.Width = (Cols + 2) * Length;
            int help = Length;
            for (int i = 0; i < Rows - 1; i++)
            {
                Line l = new Line();
                l.Stroke = new SolidColorBrush(Color.FromArgb(255, 100, 100, 100));
                l.StrokeThickness = 5;
                l.X1 = Length;
                l.Y1 = Length + help;
                l.X2 = C.ActualWidth - Length * 1.5;
                l.Y2 = l.Y1;
                C.Children.Add(l);
                help += Length;
            }
            help = Length;
            for (int i = 0; i < Cols - 1; i++)
            {
                Line l = new Line();
                l.Stroke = new SolidColorBrush(Color.FromArgb(255, 100, 100, 100));
                l.StrokeThickness = 5;
                l.X1 = Length + help;
                l.Y1 = Length;
                l.X2 = l.X1;
                l.Y2 = C.ActualHeight - Length * 1.5;
                C.Children.Add(l);
                help += Length;
            }

            Random randy = new Random();
            for (int i = 2; i < Rows + 1; i++)
            {
                for (int j = 2; j < Cols +1 ; j++)
                {
                    int random = randy.Next(0, 100);
                    if (random < 75)
                    {
                        Pill p = new Pill(new Thickness(j * Length - 5, i * Length - 5, 0, 0));
                        C.Children.Add(p.R);
                        pilllist.Add(p);
                    }
                    else
                    {
                        BigPill p = new BigPill(new Thickness(j * Length - 10, i * Length - 10, 0, 0));
                        C.Children.Add(p.R);
                        pilllist.Add(p);
                    }
                }
            }
        }
        public void Pacman_crossed(double x, double y)
        {
            for (int i = pilllist.Count-1; i >= 0; i--)
            {
                if (pilllist[i].Margin == new Thickness(x + Length - pilllist[i].Size / 2, y + Length * 2 - pilllist[i].Size / 2, 0, 0))
                {
                    Points+=pilllist[i].Points;
                    C.Children.Remove(pilllist[i].R);
                    pilllist.RemoveAt(i);
                }
            }
            if (pilllist.Count == 0)
            {
                C.Children.RemoveRange(0, C.Children.Count);
                TextBlock tb = new TextBlock();
                tb.Text = "Game Over";
                tb.FontSize = 30;
                tb.Height = 100;
                tb.Width = 320;
                C.Children.Add(tb);
                Canvas.SetLeft(tb, (C.ActualWidth-tb.Width) / 2);
                Canvas.SetTop(tb, (C.ActualHeight- tb.Height) / 2);
                DoubleAnimation db = new DoubleAnimation(60,new TimeSpan(0,0,0,3));
                tb.BeginAnimation(TextBlock.FontSizeProperty, db);
            }
        }
    }
}
