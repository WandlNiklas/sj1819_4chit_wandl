﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfAnimatedGif;

namespace Pacman_Game
{
    public class Pacman
    {
        Image i;
        int StepWidth;
        int GridWidth;
        Canvas C;
        public enum Dir { Left = 0, Up = 1, Right = 2, Down = 3 }
        public Dir d;
        public double posx, posy;
        public delegate void Cross(double x, double y);
        public event Cross crossed;
        public Pacman(int stepwidth, int grid, Canvas c)
        {
            C = c;
            StepWidth = stepwidth;
            GridWidth = grid;
            i = new Image();
            i.Loaded += I_Loaded;
            c.Children.Add(i);
            d = Dir.Right;
        }

        private void I_Loaded(object sender, RoutedEventArgs e)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(@"C:\Users\Niklas\Desktop\Schule\JAHR 4\SEW\Projekte\190116\Pacman_Game\Pacman_Game\bin\Debug\pacmananimated.gif");
            bi.EndInit();
            Image i = sender as Image;
            i.Source = bi;
            i.Height = GridWidth;
            i.Width = GridWidth;
            ImageBehavior.SetAnimatedSource(i, bi);
            double left = GridWidth * 0.75;
            Canvas.SetLeft(i, left);
            double top = GridWidth * 1.5;
            Canvas.SetTop(i, top);
            posx = i.Width/2;
            posy = 0;
        }
        public void TurnLeft()
        {
            if (d == Dir.Left || d == Dir.Right)
                d = Dir.Left;
            else
            {
                if (posy % GridWidth == 0)
                    d = Dir.Left;
            }
        }
        public void TurnUp()
        {
            if (d == Dir.Up || d == Dir.Down)
                d = Dir.Up;
            else
            {
                if (posx % GridWidth == 0)
                    d = Dir.Up;
            }
        }
        public void TurnRight()
        {
            if (d == Dir.Right || d == Dir.Left)
                d = Dir.Right;
            else
            {
                if (posy % GridWidth == 0)
                    d = Dir.Right;
            }
        }
        public void TurnDown()
        {
            if (d == Dir.Down || d == Dir.Up)
                d = Dir.Down;
            else
            {
                if (posx % GridWidth == 0)
                    d = Dir.Down;
            }
        }
        public void MoveOn(object sender, EventArgs e)
        {
            TransformGroup tg = new TransformGroup();
            RotateTransform rt = null;
            TranslateTransform tt = null;
            if(d == Dir.Right)
            {
                if (posx + StepWidth < C.ActualWidth-GridWidth*2-i.Width/2)
                    posx += StepWidth;
                rt = new RotateTransform(0);
                tt = new TranslateTransform(posx, posy);
            }
            if (d == Dir.Left)
            {
                if (posx - StepWidth > -i.Width*0.25)
                    posx -= StepWidth;
                rt = new RotateTransform(180);
                tt = new TranslateTransform(posx, posy);
                tg.Children.Add(new ScaleTransform(1, -1));
            }
            if (d == Dir.Down)
            {
                if (posy + StepWidth < C.ActualHeight - GridWidth * 2 - i.Height)
                    posy += StepWidth;
                rt = new RotateTransform(90);
                tt = new TranslateTransform(posx-i.Width/4, posy);
            }
            if (d == Dir.Up)
            {
                if (posy - StepWidth > -i.Height)
                    posy -= StepWidth;
                rt = new RotateTransform(270);
                tt = new TranslateTransform(posx - i.Width / 4, posy);
            }
            tg.Children.Add(rt);
            tg.Children.Add(tt);
            i.RenderTransformOrigin = new Point(0.5, 0.5);
            i.RenderTransform = tg;

            if (posx % GridWidth == 0 && posy % GridWidth == 0)
                crossed(posx,posy);
        }
    }
}
