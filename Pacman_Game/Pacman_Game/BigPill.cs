﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Pacman_Game
{
    class BigPill:Pill
    {
        public BigPill(Thickness s):base(s)
        {
            Margin = s;
            Size = 20;
            Color = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 200, 100));
            Points = 5;
            Rectangle r = new Rectangle();
            r.Fill = Color;
            r.Stroke = Color;
            r.Height = Size;
            r.Width = Size;
            r.Margin = Margin;
            R = r;
        }
    }
}
