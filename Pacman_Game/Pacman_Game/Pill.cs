﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Pacman_Game
{
    class Pill
    {
        public int Size { get; set; }
        public SolidColorBrush Color { get; set; }
        public Thickness Margin {get;set;}
        public int Points { get; set; }
        public Rectangle R { get; set; }

        public Pill(Thickness s)
        {
            Margin = s;
            Size = 10;
            Color = new SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0));
            Points = 1;
            Rectangle r = new Rectangle();
            r.Fill = Color;
            r.Stroke = Color;
            r.Height = Size;
            r.Width = Size;
            r.Margin = Margin;
            R = r;
        }
    }
}
