﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Pacman_Game
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Game g;
        DispatcherTimer t;
        public MainWindow()
        {
            InitializeComponent();
            DispatcherTimer updateP = new DispatcherTimer();
            updateP.Interval = new TimeSpan(0, 0, 0, 0, 100);
            t = new DispatcherTimer();
            t.Interval = updateP.Interval;
            updateP.Tick += UpdateP_Tick;
            Loaded += delegate
            {
                g = new Game(40, playGround);
                t.Tick += g.p.MoveOn;
                updateP.Start();
            };
        }

        private void UpdateP_Tick(object sender, EventArgs e)
        {
            topTb.Text = "Points: " + g.Points;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Left)
            {
                g.p.TurnLeft();
            }
            if (e.Key == Key.Up)
            {
                g.p.TurnUp();
            }
            if (e.Key == Key.Right)
            {
                g.p.TurnRight();
            }
            if (e.Key == Key.Down)
            {
                g.p.TurnDown();
            }
            if(e.Key == Key.Space)
            {
                t.Start();
                bottomTb.Text = "Have fun!";
            }
        }
    }
}
