﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace CAD_refactored
{
    abstract public class myShape
    {
        abstract public Shape Operation(Panel p);
    }
}