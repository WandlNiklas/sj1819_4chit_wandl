﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CAD_refactored
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<myShape> shapelist = new List<myShape>();
        Line l = new Line();
        Random rnd;
        public static MainWindow AppWindow;
        Shape s;
        public MainWindow()
        {
            InitializeComponent();
            rnd = new Random();
            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timer1.Tick += Timer1_Tick;
            timer1.Start();
            myGrid.Children.Add(l);
            myGrid.Children.Remove(redrawButton);
            AppWindow = this;
            cb.Items.Add("Line");
            cb.Items.Add("Rectangle");
            cb.Items.Add("Circle");
            cb.SelectedIndex = 0;
            Panel.SetZIndex(cb, 2);
            Panel.SetZIndex(lx1, 2);
            Panel.SetZIndex(lx2, 2);
            Panel.SetZIndex(ly1, 2);
            Panel.SetZIndex(ly2, 2);
            Panel.SetZIndex(tx1, 2);
            Panel.SetZIndex(tx2, 2);
            Panel.SetZIndex(ty1, 2);
            Panel.SetZIndex(ty2, 2);
            Panel.SetZIndex(lDraw, 2);
            Panel.SetZIndex(selectButton, 2);
            Panel.SetZIndex(drawButton, 2);
            Panel.SetZIndex(redrawButton, 2);
            Panel.SetZIndex(helpGrid, 1);
            Panel.SetZIndex(helpRec, 1);
            Panel.SetZIndex(menuBar, 2);
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            myGrid.Children.Remove(l);
            l = new Line();
            l.X1 = Width*0.25;
            l.X2 = Width*0.25;
            l.Y1 = 0;
            l.Y2 = Height;
            l.StrokeThickness = 10;
            l.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            myGrid.Children.Add(l);
            menuBar.Width = Width * 0.25;
            helpRec.Width = Width * 0.25;
        }
        public void button1_Click(object sender, RoutedEventArgs e)
        {
            if (!myGrid.Children.Contains(ly2))
            {
                myGrid.Children.Add(ly2);
                Panel.SetZIndex(ly2, 2);
            }
            if (!myGrid.Children.Contains(ty2))
            {
                myGrid.Children.Add(ty2);
                Panel.SetZIndex(ty2, 2);
            }
            if ((string)cb.SelectedItem == "Line")
            {
                lDraw.Content = "Draw a line:";
                lx1.Content = "X1:";
                ly1.Content = "Y1:";
                lx2.Content = "X2:";
                ly2.Content = "Y2:";
            }
            if ((string)cb.SelectedItem == "Rectangle")
            {
                lDraw.Content = "Draw a rectangle:";
                lx1.Content = "Left:";
                ly1.Content = "Top:";
                lx2.Content = "Height:";
                ly2.Content = "Width:";
            }
            if ((string)cb.SelectedItem == "Circle")
            {
                lDraw.Content = "Draw a circle:";
                lx1.Content = "Center1:";
                ly1.Content = "Center2:";
                lx2.Content = "Radius:";
                myGrid.Children.Remove(ly2);
                myGrid.Children.Remove(ty2);
            }
        }
        public void Button_Click(object sender, RoutedEventArgs e)
        {
            SolidColorBrush randomColor = new SolidColorBrush();
            randomColor.Color = Color.FromArgb(255, (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            int[] values = new int[4];
            try
            {
                values[0] = Convert.ToInt32(tx1.Text);
                values[1] = Convert.ToInt32(ty1.Text);
                values[2] = Convert.ToInt32(tx2.Text);

                if (cb.SelectedIndex != 2)
                {
                    values[3] = Convert.ToInt32(ty2.Text);
                }
            }
            catch
            {
                MessageBox.Show("Nur Ganzzahlen erlaubt!");
            }
            if (cb.SelectedIndex == 1)
            {
                try
                {
                    if (Convert.ToInt32(tx2.Text) < 0 || Convert.ToInt32(ty2.Text) < 0)
                    {
                        MessageBox.Show("Nur positive Werte für Width/Height!");
                    }
                }
                catch
                {
                    MessageBox.Show("Nur positive Werte für Width/Height!");
                }
            }
            if (cb.SelectedIndex == 2 && Convert.ToInt32(tx2.Text) < 0)
            {
                MessageBox.Show("Nur positive Werte für Radius!");
            }
            if ((string)cb.SelectedItem == "Line")
            {
                myLine l = new myLine()
                {
                    X1 = values[0],
                    Y1 = values[1],
                    X2 = values[2],
                    Y2 = values[3],
                    Color = randomColor,
                    Thickness = 10
                };
                l.Operation(myGrid);
                shapelist.Add(l);
            }
            if ((string)cb.SelectedItem == "Rectangle")
            {
                myRectangle r = new myRectangle()
                {
                    X1 = values[0],
                    Y1 = values[1],
                    Height = values[2],
                    Width = values[3],
                    Color = randomColor,
                    Thickness = 10
                };
                r.Operation(myGrid);
                shapelist.Add(r);
            }
            if ((string)cb.SelectedItem == "Circle")
            {
                myCircle c = new myCircle()
                {
                    X1 = values[0],
                    Y1 = values[1],
                    Radius = values[2],
                    Color = randomColor,
                    Thickness = 10
                };
                c.Operation(myGrid);
                shapelist.Add(c);
            }
            tx1.Text = "";
            ty1.Text = "";
            tx2.Text = "";
            ty2.Text = "";
        }
        public void MouseLeftButtonDown_Click(object sender, MouseButtonEventArgs e)
        {
            myGrid.Children.Remove(drawButton);
            if (!myGrid.Children.Contains(redrawButton))
            {
                myGrid.Children.Add(redrawButton);
            }
            if (!myGrid.Children.Contains(ly2))
            {
                myGrid.Children.Add(ly2);
            }
            if (!myGrid.Children.Contains(ty2))
            {
                myGrid.Children.Add(ty2);
            }
            if (sender is Line)
            {
                s = (Shape)sender;
                Line l = (Line)sender;
                tx1.Text = (l.X1-myGrid.ActualWidth*0.25).ToString();
                ty1.Text = l.Y1.ToString();
                tx2.Text = (l.X2 - myGrid.ActualWidth * 0.25).ToString();
                ty2.Text = l.Y2.ToString();
                lDraw.Content = "Redraw a line:";
                lx1.Content = "X1:";
                ly1.Content = "Y1:";
                lx2.Content = "X2:";
                ly2.Content = "Y2:";
                SelectedDecorator d = new SelectedDecorator();
                d.SetShape(l);
                d.Operation(myGrid);
                TextDecorator d_ = new TextDecorator();
                d_.SetShape(l);
                d_.Operation(myGrid);
            }
            if (sender is Rectangle)
            {
                s = (Shape)sender;
                Rectangle r = (Rectangle)sender;
                tx1.Text = (r.Margin.Left - myGrid.ActualWidth * 0.5 + myGrid.ActualWidth).ToString();
                ty1.Text = (r.Margin.Top+myGrid.ActualHeight).ToString();
                tx2.Text = r.Height.ToString();
                ty2.Text = r.Width.ToString();
                lDraw.Content = "Redraw a rectangle:";
                lx1.Content = "Left:";
                ly1.Content = "Top:";
                lx2.Content = "Height:";
                ly2.Content = "Width:";
                SelectedDecorator d = new SelectedDecorator();
                d.SetShape(r);
                d.Operation(myGrid);
                TextDecorator d_ = new TextDecorator();
                d_.SetShape(r);
                d_.Operation(myGrid);
            }
            if (sender is Ellipse)
            {
                s = (Shape)sender;
                Ellipse c = (Ellipse)sender;
                tx1.Text = (c.Margin.Left - myGrid.ActualWidth * 0.5 + myGrid.ActualWidth).ToString();
                ty1.Text = (c.Margin.Top + myGrid.ActualHeight).ToString();
                tx2.Text = (c.Width*0.5).ToString();
                lDraw.Content = "Redraw the circle:";
                lx1.Content = "Center1:";
                ly1.Content = "Center2:";
                lx2.Content = "Radius:";
                myGrid.Children.Remove(ly2);
                myGrid.Children.Remove(ty2);
                SelectedDecorator d = new SelectedDecorator();
                d.SetShape(c);
                d.Operation(myGrid);
                TextDecorator d_ = new TextDecorator();
                d_.SetShape(c);
                d_.Operation(myGrid);
            }
        }
        private void B_Click(object sender, RoutedEventArgs e)
        {
            helpGrid.Children.RemoveRange(0, 99);
            if (s is Line)
            {
                Line l = (Line)s;
                l.X1 = Convert.ToInt32(tx1.Text) + myGrid.ActualWidth * 0.25;
                l.Y1 = Convert.ToInt32(ty1.Text);
                l.X2 = Convert.ToInt32(tx2.Text) + myGrid.ActualWidth * 0.25;
                l.Y2 = Convert.ToInt32(ty2.Text);
            }
            if (s is Rectangle)
            {
                Rectangle r = (Rectangle)s;
                r.Margin = new Thickness(Convert.ToInt32(tx1.Text) + myGrid.ActualWidth * 0.5 - myGrid.ActualWidth, Convert.ToInt32(ty1.Text) - myGrid.ActualHeight, 0, 0);
                r.Height = Convert.ToInt32(tx2.Text);
                r.Width = Convert.ToInt32(ty2.Text);
            }
            if (s is Ellipse)
            {
                Ellipse c = (Ellipse)s;
                c.Margin = new Thickness(Convert.ToInt32(tx1.Text) + myGrid.ActualWidth * 0.5 - myGrid.ActualWidth, Convert.ToInt32(ty1.Text) - myGrid.ActualHeight, 0, 0);
                c.Width = Convert.ToInt32(tx2.Text)*2;
                c.Height = Convert.ToInt32(tx2.Text)*2;
            }
            myGrid.Children.Add(drawButton);
            myGrid.Children.Remove(redrawButton);

            tx1.Text = "";
            ty1.Text = "";
            tx2.Text = "";
            ty2.Text = "";
        }

        private void imp_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".csv";
            ofd.Filter = "CSV Files (*.csv)|*.csv";
            List<myShape> impList = new List<myShape>();
            try
            {
                if (ofd.ShowDialog() == true)
                {
                    StreamReader sr = new StreamReader(ofd.FileName);
                    while (sr.ReadLine() != null)
                    {
                        List<string> help = File.ReadAllLines(ofd.FileName).ToList();
                        foreach (string s in help)
                        {
                            string[] info = s.Split(';');
                            if (info[0] == "L")
                            {
                                SolidColorBrush helpBrush = new SolidColorBrush();
                                helpBrush.Color = (Color)ColorConverter.ConvertFromString(info[6]);
                                impList.Add(new myLine
                                {
                                    X1 = Convert.ToInt32(info[1]),
                                    Y1 = Convert.ToInt32(info[2]),
                                    X2 = Convert.ToInt32(info[3]),
                                    Y2 = Convert.ToInt32(info[4]),
                                    Thickness = Convert.ToInt32(info[5]),
                                    Color = helpBrush,
                                    CreationTime = Convert.ToDateTime(info[7])
                                });
                            }
                            if (info[0] == "R")
                            {
                                SolidColorBrush helpBrush = new SolidColorBrush();
                                helpBrush.Color = (Color)ColorConverter.ConvertFromString(info[6]);
                                impList.Add(new myRectangle
                                {
                                    X1 = Convert.ToInt32(info[1]),
                                    Y1 = Convert.ToInt32(info[2]),
                                    Width = Convert.ToInt32(info[3]),
                                    Height = Convert.ToInt32(info[4]),
                                    Thickness = Convert.ToInt32(info[5]),
                                    Color = helpBrush,
                                    CreationTime = Convert.ToDateTime(info[7])
                                });
                            }
                            if (info[0] == "C")
                            {
                                SolidColorBrush helpBrush = new SolidColorBrush();
                                helpBrush.Color = (Color)ColorConverter.ConvertFromString(info[5]);
                                impList.Add(new myCircle
                                {
                                    X1 = Convert.ToInt32(info[1]),
                                    Y1 = Convert.ToInt32(info[2]),
                                    Radius = Convert.ToInt32(info[3]),
                                    Thickness = Convert.ToInt32(info[4]),
                                    Color = helpBrush,
                                    CreationTime = Convert.ToDateTime(info[6])
                                });
                            }
                        }
                    }
                    sr.Close();
                }
                for (int i = 0; i < Math.Sqrt(impList.Count); i++)
                {
                    impList[i].Operation(myGrid);
                    shapelist.Add(impList[i]);
                }
                impList.Clear();
                MessageBox.Show("Import successful!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void exp_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = ".csv";
            sfd.Filter = "CSV Files (*.csv)|*.csv";
            try
            {
                if (sfd.ShowDialog() == true)
                {
                    StreamWriter sw = new StreamWriter(sfd.FileName);
                    foreach (myShape s in shapelist)
                    {
                        if (s is myLine)
                        {
                            myLine l = (myLine)s;
                            sw.WriteLine("L;" + l.X1 + ";" + l.Y1 + ";" + l.X2 + ";" + l.Y2 + ";" + l.Thickness + ";" + l.Color + ";" + l.CreationTime);
                            sw.Flush();
                        }
                        if (s is myRectangle)
                        {
                            myRectangle r = (myRectangle)s;
                            sw.WriteLine("R;" + r.X1 + ";" + r.Y1 + ";" + r.Width + ";" + r.Height + ";" + r.Thickness + ";" + r.Color + ";" + r.CreationTime);
                            sw.Flush();
                        }
                        if (s is myCircle)
                        {
                            myCircle c = (myCircle)s;
                            sw.WriteLine("C;" + c.X1 + ";" + c.Y1 + ";" + c.Radius + ";" + c.Thickness + ";" + c.Color + ";" + c.CreationTime);
                            sw.Flush();
                        }
                    }
                    sw.Close();
                }
                MessageBox.Show("Export successful!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
