﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace CAD_refactored
{
    class TextDecorator : Decorator, IDecorator
    {
        public override Shape Operation(Panel p)
        {
            if (S is Line)
            {
                Line help = (Line)S;
                Label l1 = new Label();
                l1.Margin = new Thickness(help.X1 - 10, help.Y1 - 10, 0, 0);
                l1.Content = "X1: " + (help.X1 - MainWindow.AppWindow.myGrid.ActualWidth * 0.25) + " Y1: " + help.Y1;
                l1.HorizontalAlignment = HorizontalAlignment.Left;
                l1.VerticalAlignment = VerticalAlignment.Top;
                MainWindow.AppWindow.helpGrid.Children.Add(l1);
                Label l2 = new Label();
                l2.HorizontalAlignment = HorizontalAlignment.Left;
                l2.VerticalAlignment = VerticalAlignment.Top;
                l2.Margin = new Thickness(help.X2 + 10, help.Y2 + 10, 0, 0);
                l2.Content = "X2: " + (help.X2 - MainWindow.AppWindow.myGrid.ActualWidth * 0.25) + " Y2: " + help.Y2;
                MainWindow.AppWindow.helpGrid.Children.Add(l2);
            }
            if (S is Rectangle)
            {
                Rectangle help = (Rectangle)S;
                Label l1 = new Label();
                l1.Content = "Left: " + (help.Margin.Left - MainWindow.AppWindow.myGrid.ActualWidth * 0.5 + MainWindow.AppWindow.myGrid.ActualWidth) + " Right: " + (help.Margin.Top + MainWindow.AppWindow.myGrid.ActualHeight) + "\nWidth: " + help.Width + " Height: " + help.Height;
                l1.Margin = new Thickness(help.Margin.Left, help.Margin.Top, 0, 0);
                l1.HorizontalAlignment = HorizontalAlignment.Center;
                l1.VerticalAlignment = VerticalAlignment.Center;
                MainWindow.AppWindow.helpGrid.Children.Add(l1);
            }
            if (S is Ellipse)
            {
                Ellipse help = (Ellipse)S;
                Label l1 = new Label();
                l1.Content = "Center1: " + (help.Margin.Left - MainWindow.AppWindow.myGrid.ActualWidth * 0.5 + MainWindow.AppWindow.myGrid.ActualWidth) + " Center2: " + (help.Margin.Top + MainWindow.AppWindow.myGrid.ActualHeight);
                l1.Margin = new Thickness(help.Margin.Left, help.Margin.Top, 0, 0);
                l1.HorizontalAlignment = HorizontalAlignment.Center;
                l1.VerticalAlignment = VerticalAlignment.Center;
                MainWindow.AppWindow.helpGrid.Children.Add(l1);
            }
            return null;
        }
    }
}