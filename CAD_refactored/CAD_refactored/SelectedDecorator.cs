﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CAD_refactored
{
    class SelectedDecorator : Decorator, IDecorator
    {
        public override Shape Operation(Panel p)
        {
            if (S is Line)
            {
                SolidColorBrush scb = new SolidColorBrush();
                scb.Color = Colors.Gray;
                Rectangle r1 = new Rectangle();
                Line help = (Line)S;
                r1.Width = Math.Abs(help.X2 - help.X1 + 10);
                r1.Height = 10;
                r1.Fill = scb;
                r1.StrokeThickness = 1;
                if (help.X1 > help.X2)
                {
                    r1.Width += 10;
                    r1.Margin = new Thickness(help.X1 - 5 - r1.Width, help.Y1 - 5, 0, 0);
                }
                else
                {
                    r1.Margin = new Thickness(help.X1 - 5, help.Y1 - 5, 0, 0);
                }
                r1.HorizontalAlignment = HorizontalAlignment.Left;
                r1.VerticalAlignment = VerticalAlignment.Top;
                MainWindow.AppWindow.helpGrid.Children.Add(r1);
                Rectangle r2 = new Rectangle();
                r2.Width = Math.Abs(help.X2 - help.X1 + 10);
                r2.Height = 10;
                r2.Fill = scb;
                r2.StrokeThickness = 1;
                r2.HorizontalAlignment = HorizontalAlignment.Left;
                r2.VerticalAlignment = VerticalAlignment.Top;
                if (help.X1 > help.X2)
                {
                    r2.Width += 10;
                    r2.Margin = new Thickness(help.X1 - 5 - r2.Width, help.Y2 - 5, 0, 0);
                }
                else
                {
                    r2.Margin = new Thickness(help.X1 - 5, help.Y2 - 5, 0, 0);
                }
                MainWindow.AppWindow.helpGrid.Children.Add(r2);
                Rectangle r3 = new Rectangle();
                r3.Width = 10;
                r3.Height = Math.Abs(help.Y2 - help.Y1 + 10);
                r3.Fill = scb;
                r3.StrokeThickness = 1;
                if (help.Y1 > help.Y2)
                {
                    r3.Height += 20;
                    r3.Margin = new Thickness(help.X1 - 5, help.Y1 - r3.Height + 5, 0, 0);
                }
                else
                {
                    r3.Margin = new Thickness(help.X1 - 5, help.Y1 - 5, 0, 0);
                }
                r3.HorizontalAlignment = HorizontalAlignment.Left;
                r3.VerticalAlignment = VerticalAlignment.Top;
                MainWindow.AppWindow.helpGrid.Children.Add(r3);
                Rectangle r4 = new Rectangle();
                r4.Width = 10;
                r4.Height = Math.Abs(help.Y2 - help.Y1 + 10);
                r4.Fill = scb;
                r4.StrokeThickness = 1;
                r4.HorizontalAlignment = HorizontalAlignment.Left;
                r4.VerticalAlignment = VerticalAlignment.Top;
                if (help.Y1 > help.Y2)
                {
                    r4.Height += 10;
                    r4.Margin = new Thickness(help.X2 - 5, help.Y1 - r4.Height - 5, 0, 0);
                }
                else
                {
                    r4.Margin = new Thickness(help.X2 - 5, help.Y1 - 5, 0, 0);
                }
                MainWindow.AppWindow.helpGrid.Children.Add(r4);
            }
            if (S is Rectangle)
            {
                SolidColorBrush scb = new SolidColorBrush();
                scb.Color = Colors.Gray;
                Rectangle help = (Rectangle)S;
                Rectangle r1 = new Rectangle();
                r1.Width = 10;
                r1.Height = help.Height + 10;
                r1.Fill = scb;
                r1.StrokeThickness = 1;
                r1.Margin = new Thickness(help.Margin.Left - help.Width, help.Margin.Top, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r1);
                Rectangle r2 = new Rectangle();
                r2.Width = help.Width + 10;
                r2.Height = 10;
                r2.Fill = scb;
                r2.StrokeThickness = 1;
                r2.Margin = new Thickness(help.Margin.Left, help.Margin.Top - help.Height, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r2);
                Rectangle r3 = new Rectangle();
                r3.Width = 10;
                r3.Height = help.Height + 10;
                r3.Fill = scb;
                r3.StrokeThickness = 1;
                r3.Margin = new Thickness(help.Margin.Left + help.Width, help.Margin.Top, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r3);
                Rectangle r4 = new Rectangle();
                r4.Width = help.Width + 10;
                r4.Height = 10;
                r4.Fill = scb;
                r4.StrokeThickness = 1;
                r4.Margin = new Thickness(help.Margin.Left, help.Margin.Top + help.Height, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r4);
            }
            if (S is Ellipse)
            {
                SolidColorBrush scb = new SolidColorBrush();
                scb.Color = Colors.Gray;
                Ellipse help = (Ellipse)S;
                Rectangle r1 = new Rectangle();
                r1.Width = 10;
                r1.Height = help.Height + 10;
                r1.Fill = scb;
                r1.StrokeThickness = 1;
                r1.Margin = new Thickness(help.Margin.Left - help.Width, help.Margin.Top, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r1);
                Rectangle r2 = new Rectangle();
                r2.Width = help.Width + 10;
                r2.Height = 10;
                r2.Fill = scb;
                r2.StrokeThickness = 1;
                r2.Margin = new Thickness(help.Margin.Left, help.Margin.Top - help.Height, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r2);
                Rectangle r3 = new Rectangle();
                r3.Width = 10;
                r3.Height = help.Height + 10;
                r3.Fill = scb;
                r3.StrokeThickness = 1;
                r3.Margin = new Thickness(help.Margin.Left + help.Width, help.Margin.Top, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r3);
                Rectangle r4 = new Rectangle();
                r4.Width = help.Width + 10;
                r4.Height = 10;
                r4.Fill = scb;
                r4.StrokeThickness = 1;
                r4.Margin = new Thickness(help.Margin.Left, help.Margin.Top + help.Height, 0, 0);
                MainWindow.AppWindow.helpGrid.Children.Add(r4);
            }
            return null;
        }
    }
}