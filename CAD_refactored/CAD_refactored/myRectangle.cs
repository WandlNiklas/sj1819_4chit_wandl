﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CAD_refactored
{
    public class myRectangle : myShape, IShape
    {
        public int X1 { get; set; }

        public int Y1 { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public int Thickness { get; set; }

        public SolidColorBrush Color { get; set; }
        public DateTime CreationTime { get; set; }

        public override Shape Operation(Panel p)
        {
            Rectangle myRectangle = new Rectangle();
            myRectangle.Fill = Color;

            myRectangle.StrokeThickness = Thickness;
            myRectangle.Stroke = Color;

            myRectangle.Width = Width;
            myRectangle.Height = Height;
            Panel.SetZIndex(myRectangle, 0);
            myRectangle.MouseLeftButtonDown += MainWindow.AppWindow.MouseLeftButtonDown_Click;
            myRectangle.Margin = new Thickness(X1 + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth, Y1 - MainWindow.AppWindow.myGrid.ActualHeight, 0, 0);
            p.Children.Add(myRectangle);
            CreationTime = DateTime.Now.Date;
            return myRectangle;
        }
    }
}