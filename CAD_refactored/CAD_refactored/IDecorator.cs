﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CAD_refactored
{
    public interface IDecorator : IShape
    {
        void Operation();
        void SetShape();
    }
}