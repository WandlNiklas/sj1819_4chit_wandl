﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CAD_refactored
{
    public class myLine : myShape, IShape
    {
        public int X1 { get; set; }

        public int X2 { get; set; }

        public int Y1 { get; set; }

        public int Y2 { get; set; }

        public SolidColorBrush Color { get; set; }

        public int Thickness { get; set; }
        public DateTime CreationTime { get; set; }
        public override Shape Operation(Panel p)
        {
            Line myLine = new Line();
            myLine.Stroke = Color;

            myLine.X1 = X1 + MainWindow.AppWindow.myGrid.ActualWidth * 0.25;
            myLine.Y1 = Y1;
            myLine.X2 = X2 + MainWindow.AppWindow.myGrid.ActualWidth * 0.25;
            myLine.Y2 = Y2;

            myLine.StrokeThickness = Thickness;
            myLine.MouseLeftButtonDown += MainWindow.AppWindow.MouseLeftButtonDown_Click;
            Panel.SetZIndex(myLine, 0);
            p.Children.Add(myLine);
            CreationTime = DateTime.Now.Date;
            return myLine;
        }
    }
}