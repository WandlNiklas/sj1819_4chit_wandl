﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CAD_refactored
{
    public class myCircle : myShape, IShape
    {
        public int X1 { get; set; }

        public int Y1 { get; set; }

        public int Radius { get; set; }

        public int Thickness { get; set; }

        public SolidColorBrush Color { get; set; }
        public DateTime CreationTime { get; set; }

        public override Shape Operation(Panel p)
        {
            Ellipse myEllipse = new Ellipse();
            myEllipse.Fill = Color;

            myEllipse.StrokeThickness = Thickness;
            myEllipse.Stroke = Color;

            myEllipse.Width = Radius * 2;
            myEllipse.Height = Radius * 2;
            Panel.SetZIndex(myEllipse, 0);
            myEllipse.MouseLeftButtonDown += MainWindow.AppWindow.MouseLeftButtonDown_Click;
            myEllipse.Margin = new Thickness(X1 + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth, Y1 - MainWindow.AppWindow.myGrid.ActualHeight, 0, 0);
            p.Children.Add(myEllipse);
            CreationTime = DateTime.Now.Date;
            return myEllipse;
        }
    }
}