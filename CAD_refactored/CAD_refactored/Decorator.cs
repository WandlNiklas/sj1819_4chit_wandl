﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Shapes;

namespace CAD_refactored
{
    abstract class Decorator : myShape
    {
        public Shape S { get; set; }
        public void SetShape(Shape s)
        {
            this.S = s;
        }
        public override Shape Operation(Panel p)
        {
            return null;
        }
    }
}