﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using blablacar;

namespace blablacarTests
{
    [TestClass]
    public class BlaBlaCarTests
    {
        User u = new User { ID = 1, Name = "Test", Info = "test", Geburtsdatum = DateTime.Now, Email = "test", Handynummer = "test" };
        [TestMethod]
        public void UserAnlegen()
        {
            Manager m = new Manager();
            
            m.CreateUser(u);
            User test = m.GetUsers()[m.GetUsers().Count-1];

            Assert.AreEqual(u.ID, test.ID);
        }
        [TestMethod]
        public void UserBearbeiten()
        {
            Manager m = new Manager();
            User u = new User { ID = 2, Name = "Test", Info = "test", Geburtsdatum = DateTime.Now, Email = "test", Handynummer = "test" };
            m.CreateUser(u);
            u.Name = "Test 2";
            m.EditUser();

            Assert.AreEqual("Test 2", m.GetUsers().Find(v => v.ID==u.ID).Name);
        }
        [TestMethod]
        public void UserLoeschen()
        {
            Manager m = new Manager();
            int users = m.GetUsers().Count;
            User u = new User { ID = 3, Name = "Test 3", Info = "test", Geburtsdatum = DateTime.Now, Email = "test", Handynummer = "test" };
            m.CreateUser(u);
            m.DeleteUser(u);

            Assert.AreEqual(users,m.GetUsers().Count);
        }
        [TestMethod]
        public void FahrtAnlegen()
        {
            Manager m = new Manager();
            u.UpgradeToAnbieter();
            Fahrt f = new Fahrt { ID = 1,Von="testv",Nach="testn",Preis=22.5,Mitfahrer=null,Sitzplaetze=4,Status="unangetreten",Wann=DateTime.Now,Rückfahrt=null,Anbieter=u.me };

            m.CreateFahrt(u.me,f);
            Fahrt test = m.GetFahrten()[m.GetFahrten().Count - 1];

            Assert.AreEqual(u.ID, test.ID);
        }
        [TestMethod]
        public void FahrtBearbeiten()
        {
            Manager m = new Manager();
            u.UpgradeToAnbieter();
            Fahrt f = new Fahrt { ID = 2, Von = "testv", Nach = "testn", Preis = 22.5, Mitfahrer = null, Sitzplaetze = 4, Status = "unangetreten", Wann = DateTime.Now, Rückfahrt = null, Anbieter = u.me };
            m.CreateFahrt(u.me,f);
            f.Status = "angetreten";
            m.EditFahrt(u.me);

            Assert.AreEqual("angetreten", m.GetFahrten().Find(v => v.ID == u.ID).Status);
        }
        [TestMethod]
        public void FahrtLoeschen()
        {
            Manager m = new Manager();
            int fahrten = m.GetFahrten().Count;
            u.UpgradeToAnbieter();
            Fahrt f = new Fahrt { ID = 3, Von = "testv", Nach = "testn", Preis = 22.5, Mitfahrer = null, Sitzplaetze = 4, Status = "unangetreten", Wann = DateTime.Now, Rückfahrt = null, Anbieter = u.me };
            m.CreateFahrt(u.me, f);
            m.DeleteFahrt(u.me,f);

            Assert.AreEqual(fahrten, m.GetFahrten().Count);
        }
    }
}
