﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blablacar
{
    public class User
    {
        public string Name { get; set; }

        public int ID { get; set; }

        public string Info { get; set; }

        public DateTime Geburtsdatum { get; set; }

        public string Email { get; set; }

        public string Handynummer { get; set; }

        public List<string> Bewertungen = new List<string>();

        public Anbieter me = null;

        public void UpgradeToAnbieter()
        {
            Console.Write("Wie würden Sie gerne Ihre Zahlungen erhalten?");
            string z = Console.ReadLine();
            me = new Anbieter();
        }

        public void FahrtSuchen()
        {

        }

        public void FahrtAuswaehlen()
        {

        }

        public void Absagen()
        {

        }
    }
}