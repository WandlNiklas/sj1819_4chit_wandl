﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blablacar
{
    public class Anbieter : User
    {
        public List<string> Autodetails = new List<string>();

        public List<Fahrt> Fahrten = new List<Fahrt>();

        public string Zahlungsart { get; set; }

        public Anbieter() { 
            AddCarDetails();
        }

        public void AddCarDetails()
        {
            Console.WriteLine("How many details do you want do add?");
            int c = Convert.ToInt32(Console.ReadLine());
            for (int i = 1; i <= c; i++)
            {
                Console.Write("Enter Detail #{0}: ",i);
                Autodetails.Add(Console.ReadLine());
            }
            
        }

        public void FahrtAnbieten(Fahrt f)
        {
            Fahrten.Add(f);
        }

        public void FahrtBearbeiten()
        {

        }

        public void FahrtAbsagen(Fahrt f)
        {
            Fahrten.Remove(f);
        }

        public void MitfahrerAuswaehlen()
        {

        }

        public void FahrtAngetreten()
        {

        }

        public void FahrtAbgeschlossen()
        {

        }

        public void MitfahrerBewerten()
        {

        }
        
        public void Panne()
        {

        }

        public void RueckfahrtAbsage()
        {

        }
    }
}