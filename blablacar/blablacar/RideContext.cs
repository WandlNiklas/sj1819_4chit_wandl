﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blablacar
{
    class RideContext:DbContext
    {
        public RideContext() : base()
        {

        }

        public DbSet<User> Users { get; set; }
        public DbSet<Fahrt> Fahrten { get; set; }
    }
}
