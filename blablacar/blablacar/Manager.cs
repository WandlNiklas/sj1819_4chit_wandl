﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace blablacar
{
    public class Manager
    {
        RideContext blablacar;
        public Manager()
        {
            blablacar = new RideContext();
        }

        public void CreateUser(User user)
        {
            blablacar.Users.Add(user);
            blablacar.SaveChanges();
        }
        public void DeleteUser(User user)
        {
            blablacar.Users.Remove(user);
            blablacar.SaveChanges();
        }
        public void EditUser()
        {
            blablacar.SaveChanges();
        }
        public List<User> GetUsers()
        {
            return blablacar.Users.ToList();
        }
        public void CreateFahrt(Anbieter anbieter, Fahrt fahrt)
        {
            anbieter.FahrtAnbieten(fahrt);
            blablacar.Fahrten.Add(fahrt);
            blablacar.SaveChanges();
        }
        public void DeleteFahrt(Anbieter anbieter,Fahrt fahrt)
        {
            anbieter.FahrtAbsagen(fahrt);
            blablacar.Fahrten.Remove(fahrt);
            blablacar.SaveChanges();
        }
        public void EditFahrt(Anbieter anbieter)
        {
            anbieter.FahrtBearbeiten();
            blablacar.SaveChanges();
        }
        public List<Fahrt> GetFahrten()
        {
            return blablacar.Fahrten.ToList();
        }
    }
}
