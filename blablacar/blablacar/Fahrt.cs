﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace blablacar
{
    public class Fahrt
    {
        public int ID { get; set; }

        public string Von { get; set; }

        public string Nach { get; set; }

        public DateTime Wann { get; set; }

        public double Preis { get; set; }

        public int Sitzplaetze { get; set; }

        public Anbieter Anbieter { get; set; }

        public string Status { get; set; }

        public ICollection<User> Mitfahrer { get; set; }

        public Fahrt Rückfahrt = null;
        
    }
}