﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace Number2Text
{
    class Program
    {
        static void Main(string[] args)
        {
            long i;
            Util_ u = new Util_();
            while (true)
            {
                
                Console.Write("Number: ");
                try
                {
                    i = Convert.ToInt64(Console.ReadLine());
                    if (i < 0)
                        throw new N2TException("The number is lower than 0!");
                    else if (i > 999999)
                        throw new N2TException("The number is higher than 999999!");
                    Console.WriteLine(u.Conv2Text(i));
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
}
