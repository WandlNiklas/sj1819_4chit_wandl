﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Number2Text
{
    class Utility
    {
        Dictionary<long, string> dic = new Dictionary<long, string>();

        public Utility()
        {
            //dic.Add(0, "null");
            dic.Add(1, "ein");
            dic.Add(2, "zwei");
            dic.Add(3, "drei");
            dic.Add(4, "vier");
            dic.Add(5, "fünf");
            dic.Add(6, "sechs");
            dic.Add(7, "sieben");
            dic.Add(8, "acht");
            dic.Add(9, "neun");
            dic.Add(10, "zehn");
            dic.Add(11, "elf");
            dic.Add(12, "zwölf");
            dic.Add(13, "dreizehn");
            dic.Add(14, "vierzehn");
            dic.Add(15, "fünfzehn");
            dic.Add(16, "sechzehn");
            dic.Add(17, "siebzehn");
            dic.Add(18, "achtzehn");
            dic.Add(19, "neunzehn");
        }

        public string Conv2Text(long number)
        {
            string value = "";
            if (number == 0)
                return "null";
            if (number / 1000 > 0) 
            {
                if(dic.ContainsKey(number))
                    value += dic[number / 1000] + "tausend";
                else
                {
                    long newnumber = number / 1000;
                    if (newnumber / 100 > 0)
                    {
                        value += dic[newnumber / 100] + "hundert";
                        newnumber %= 100;
                    }
                    if (newnumber > 0)
                    {
                        if (dic.ContainsKey(newnumber))
                        {
                            value += dic[newnumber];
                            if (newnumber == 1)
                                value += "s";
                        }
                        else
                        {
                            if (newnumber / 10 > 0)
                            {
                                if (newnumber % 10 != 0)
                                    value += dic[newnumber % 10] + "und";
                                if (newnumber / 10 == 2)
                                    value += "zwanzig";
                                else if (newnumber / 10 == 3)
                                    value += "dreißig";
                                else if (newnumber / 10 == 6)
                                    value += "sechzig";
                                else if (newnumber / 10 == 7)
                                    value += "siebzig";
                                else
                                    value += dic[newnumber / 10] + "zig";
                            }
                            else
                            {
                                value += dic[newnumber];
                            }
                        }
                    }
                    value += "tausend";
                }
                number %= 1000;

            }
            if (number / 100 > 0)
            {
                value += dic[number/100] + "hundert";
                number %= 100;
            }
            if(number > 0)
            {
                if (dic.ContainsKey(number))
                {
                    value += dic[number];
                    if (number == 1)
                        value += "s";
                }
                else
                {
                    if (number / 10 > 0)
                    {
                        if (number % 10 != 0)
                            value += dic[number % 10] + "und";
                        if (number / 10 == 2)
                            value += "zwanzig";
                        else if (number / 10 == 3)
                            value += "dreißig";
                        else if (number / 10 == 6)
                            value += "sechzig";
                        else if (number / 10 == 7)
                            value += "siebzig";
                        else
                            value += dic[number / 10] + "zig";
                    }
                    else
                    {
                        value += dic[number];
                    }
                }
            }
            return value;
        }
    }
}
