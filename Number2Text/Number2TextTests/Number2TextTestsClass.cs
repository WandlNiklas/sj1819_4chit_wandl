﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Util;

namespace Number2TextTests
{
    [TestClass]
    public class Number2TextTestsClass
    {
        [TestMethod]
        public void TestMethodEleven()
        {
            Util_ u = new Util_();
            long i = 11;
            string expected = "elf";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethodTwelve()
        {
            Util_ u = new Util_();
            long i = 12;
            string expected = "zwölf";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethodTwenty()
        {
            Util_ u = new Util_();
            long i = 20;
            string expected = "zwanzig";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethodThirty()
        {
            Util_ u = new Util_();
            long i = 30;
            string expected = "dreißig";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethodSixty()
        {
            Util_ u = new Util_();
            long i = 60;
            string expected = "sechzig";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethodSeventy()
        {
            Util_ u = new Util_();
            long i = 70;
            string expected = "siebzig";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethodZero()
        {
            Util_ u = new Util_();
            long i = 0;
            string expected = "null";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestMethodOneLastNumber()
        {
            Util_ u = new Util_();
            long i = 800001;
            string expected = "achthunderttausendeins";

            string actual = u.Conv2Text(i);

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        [ExpectedException(typeof(N2TException))]
        public void TestMethodLessThanZero()
        {
            Util_ u = new Util_();
            long i = -1;

            if(i < 0)
                throw new N2TException("The number is lower than 0!");
            string actual = u.Conv2Text(i);
        }
        [TestMethod]
        [ExpectedException(typeof(N2TException))]
        public void TestMethodBiggerThan()
        {
            Util_ u = new Util_();
            long i = 1000000;

            if (i > 999999)
                throw new N2TException("The number is lower than 999999!");
            string actual = u.Conv2Text(i);
        }
    }
}
