﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Util
{
    public class N2TException:ApplicationException
    {
        public N2TException() { }
        public N2TException(string message) : base(message) { }
        public N2TException(string message, Exception inner) : base(message, inner) { }
    }
}
