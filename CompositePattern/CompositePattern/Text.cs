﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompositePattern
{
    class Text:Grafik
    {
        Font fo;
        string s;
        Label l;
        public override void Draw(int Level)
        {
            base.C.Controls.Add(l);
        }
        public Text(Control c, int top, int left, string s, Color col) : base(c, col, left, top)
        {
            this.s = s;
            this.fo = new Font("Times New Roman", 10, FontStyle.Bold);
            l = new Label { Font = fo, Top = top, Left = left, ForeColor = col, Text = s, Width = 200 };
        }
    }
}
