﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompositePattern
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Picture bg = new Picture(this, 20, 20, 500, 500, Color.FromName("White"));
            Text header = new Text(this, 20, 540, "Vergleich der Bezirke", Color.FromName("Black"));
            bg.Add(header);

            Picture krems = new Picture(this, 40, 40, 460, 80, Color.FromName("Red"));
            Text kremsText = new Text(this, 40, 560, "Bevölkerung: Krems: 20.000", Color.FromName("Red"));
            bg.Add(kremsText);
            Picture zwettl = new Picture(this, 80, 240, 460, 80, Color.FromName("Green"));
            Text zwettlText = new Text(this, 120, 560, "Bevölkerung: Zwettl: 16.000", Color.FromName("Green"));
            bg.Add(zwettlText);

            Picture euk = new Picture(this, 160, 10, 300, 20, Color.FromName("Blue"));
            Text eukText = new Text(this, 60, 580, "Einzelunternehmen(EPU): 150", Color.FromName("Blue"));
            bg.Add(eukText);
            Picture muk = new Picture(this, 200, 30, 360, 20, Color.FromName("Purple"));
            Text mukText = new Text(this, 80, 580, "Mittelunternehmen(KMU): 120", Color.FromName("Purple"));
            bg.Add(mukText);
            Picture guk = new Picture(this, 330, 50, 180, 20, Color.FromName("Brown"));
            Text gukText = new Text(this, 100, 580, "Großunternehmen(GU): 50", Color.FromName("Brown"));
            bg.Add(gukText);

            Picture euz = new Picture(this, 260, 10, 200, 20, Color.FromName("Blue"));
            Text euzText = new Text(this, 140, 580, "Einzelunternehmen(EPU): 100", Color.FromName("Blue"));
            bg.Add(euzText);
            Picture muz = new Picture(this, 160, 30, 300, 20, Color.FromName("Purple"));
            Text muzText = new Text(this, 160, 580, "Mittelunternehmen(KMU): 150", Color.FromName("Purple"));
            bg.Add(muzText);
            Picture guz = new Picture(this, 350, 50, 160, 20, Color.FromName("Brown"));
            Text guzText = new Text(this, 180, 580, "Großunternehmen(GU): 20", Color.FromName("Brown"));
            bg.Add(guzText);

            bg.Add(krems);
            krems.Add(euk);
            krems.Add(muk);
            krems.Add(guk);
            bg.Add(zwettl);
            zwettl.Add(euz);
            zwettl.Add(muz);
            zwettl.Add(guz);
            bg.Draw(0);
        }
    }
}
