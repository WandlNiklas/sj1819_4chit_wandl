﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompositePattern
{
    class Picture:Grafik
    {
        int width, height;
        Panel p;
        List<Grafik> children;

        public override void Draw(int Level)
        {
            base.C.Controls.Add(p);
            foreach (Grafik item in children)
                item.Draw(0);
        }
        public override void Add(Grafik g)
        {
            children.Add(g);
            if (g is Picture)
                g.C = p;
        }
        public Picture(Control c, int top, int left, int height, int width, Color col) : base(c, col, left, top)
        {
            this.height = height;
            this.width = width;
            children = new List<Grafik>();
            p = new Panel { BackColor = base.col, Top = base.top, Left = base.left, Height = this.height, Width = this.width };
        }
    }
}
