﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CompositePattern
{
    abstract class Grafik
    {
        public Control C { get; set; }
        public Color col;
        public int left, top;
        public Grafik(Control c, Color col, int left, int top)
        {
            this.left = left;
            this.top = top;
            this.C = c;
            this.col = col;
        }
        public virtual void Add(Grafik g)
        {

        }
        public abstract void Draw(int Level);
    }
}
