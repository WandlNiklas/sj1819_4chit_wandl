﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;
using System.Xml;

namespace TransferDBXML
{
    class Program
    {
        static void Main(string[] args)
        {
            CreateFileAll();
            CheckFile();
        }
        static public string getPatients()
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\Niklas\Desktop\Schule\JAHR 4\SEW\Projekte\190213\TransferDBXML\TransferDBXML\bin\Debug\Transfer.accdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            StringBuilder patients = new StringBuilder();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from patient";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                patients.Append(reader[0]+";"+reader[1]+";"+reader[2]+";"+reader[3]+"\n");
            }
            reader.Close();

            con.Close();
            return patients.ToString();
        }

        static public void CreateXML()
        {
            string[] patientlist = getPatients().Split('\n');
            
            XmlDocument doc = new XmlDocument(); //keine XDOcument, XNode,... verwenden
            XmlDocument docA = new XmlDocument();

            doc.LoadXml("<patients></patients>");
            docA.LoadXml("<patients></patients>");

            XmlNode root = doc.SelectSingleNode("patients");
            XmlNode rootA = docA.SelectSingleNode("patients");

            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.InsertBefore(dec,root);
            XmlDeclaration decA = docA.CreateXmlDeclaration("1.0", "UTF-8", null);
            docA.InsertBefore(decA, rootA);

            foreach (string s in patientlist)
            {
                string[] help = s.Split(';');
                if (help.Length == 4)
                {
                    //normal
                    XmlNode p = doc.CreateElement("patient");
                    XmlNode id = doc.CreateElement("id"); id.InnerText = help[0];
                    XmlNode ln = doc.CreateElement("lastname"); ln.InnerText = help[1];
                    XmlNode vn = doc.CreateElement("firstname"); vn.InnerText = help[2];
                    XmlNode sv = doc.CreateElement("svnr"); sv.InnerText = help[3];
                    p.AppendChild(id);
                    p.AppendChild(ln);
                    p.AppendChild(vn);
                    p.AppendChild(sv);
                    root.AppendChild(p);

                    //attributes
                    XmlNode pa = docA.CreateElement("patient");
                    XmlAttribute ida = docA.CreateAttribute("id"); ida.Value = help[0];
                    XmlAttribute lna = docA.CreateAttribute("lastname"); lna.Value = help[1];
                    XmlAttribute vna = docA.CreateAttribute("firstname"); vna.Value = help[2];
                    XmlAttribute sva = docA.CreateAttribute("svnr"); sva.Value = help[3];
                    pa.Attributes.Append(ida);
                    pa.Attributes.Append(lna);
                    pa.Attributes.Append(vna);
                    pa.Attributes.Append(sva);
                    rootA.AppendChild(pa);
                }
            }
            docA.Save("patientAttributes.xml");
            doc.Save("patient.xml");
        }

        static public string getPatientBehandlungen()
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\Niklas\Desktop\Schule\JAHR 4\SEW\Projekte\190213\TransferDBXML\TransferDBXML\bin\Debug\Transfer.accdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            StringBuilder pBehandlungen = new StringBuilder();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from patientbehandlungen";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                pBehandlungen.Append(reader[0] + ";" + reader[1] + ";" + reader[2] + ";" + reader[3] + "\n");
            }
            reader.Close();

            con.Close();
            return pBehandlungen.ToString();
        }
        static public string getBehandlungen()
        {
            string cs = @"Provider = Microsoft.ACE.OLEDB.12.0; Data Source = C:\Users\Niklas\Desktop\Schule\JAHR 4\SEW\Projekte\190213\TransferDBXML\TransferDBXML\bin\Debug\Transfer.accdb";
            OleDbConnection con = new OleDbConnection(cs);
            con.Open();
            StringBuilder Behandlungen = new StringBuilder();
            OleDbCommand cmd = new OleDbCommand();
            cmd.Connection = con;
            cmd.CommandText = "select * from behandlung";
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Behandlungen.Append(reader[0] + ";" + reader[1] + "\n");
            }
            reader.Close();

            con.Close();
            return Behandlungen.ToString();
        }
        static public void CreateFileAll()
        {
            string[] patientlist = getPatients().Split('\n');
            string[] bPatientlist = getPatientBehandlungen().Split('\n');
            string[] behandlunglist = getBehandlungen().Split('\n');

            XmlDocument doc = new XmlDocument(); 

            doc.LoadXml("<patients></patients>");

            XmlNode root = doc.SelectSingleNode("patients");

            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.InsertBefore(dec, root);

            foreach (string s in patientlist)
            {
                XmlNode p = doc.CreateElement("patient");
                string[] helpP = s.Split(';');
                if (helpP.Length == 4)
                {
                    XmlNode id = doc.CreateElement("id"); id.InnerText = helpP[0];
                    XmlNode ln = doc.CreateElement("lastname"); ln.InnerText = helpP[1];
                    XmlNode vn = doc.CreateElement("firstname"); vn.InnerText = helpP[2];
                    XmlNode sv = doc.CreateElement("svnr"); sv.InnerText = helpP[3];
                    p.AppendChild(id);
                    p.AppendChild(ln);
                    p.AppendChild(vn);
                    p.AppendChild(sv);
                    root.AppendChild(p);
                }
                foreach (string t in bPatientlist)
                {
                    XmlNode bh = doc.CreateElement("behandlung");
                    string[] helpPB = t.Split(';');
                    if (helpPB.Length == 4)
                    {
                        if (helpPB[0] == helpP[0])
                        {
                            XmlAttribute da = doc.CreateAttribute("dauer"); da.Value = helpPB[2];
                            XmlAttribute dt = doc.CreateAttribute("datum"); dt.Value = helpPB[3];
                            bh.Attributes.Append(da);
                            bh.Attributes.Append(dt);
                            p.AppendChild(bh);
                        }
                        foreach (string u in behandlunglist)
                        {
                            string[] helpB = u.Split(';');
                            if (helpPB[1] == helpB[0])
                                bh.InnerText = helpB[1];
                        }
                    }
                }
            }
            doc.Save("patientAll.xml");
        }
        static void CheckFile()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("patientAll.xml");
            int patientcounter = 0;

            string[] patientlist = getPatients().Split('\n');
            string[] bPatientlist = getPatientBehandlungen().Split('\n');
            string[] behandlunglist = getBehandlungen().Split('\n');

            foreach (XmlNode p in doc.SelectNodes("//patient"))
            {
                    string[] help = patientlist[patientcounter].Split(';');
                    string id = p.SelectSingleNode("id").InnerText;
                    string nachname = p.SelectSingleNode("lastname").InnerText;
                    string vorname = p.SelectSingleNode("firstname").InnerText;
                    string svnr = p.SelectSingleNode("svnr").InnerText;
                    string behandlung = null;
                    if (p.SelectSingleNode("behandlung") != null)
                        behandlung = p.SelectSingleNode("behandlung").InnerText;
                    foreach (XmlNode b in doc.SelectNodes("//behandlung"))
                    {
                        string dauer = b.Attributes["dauer"].Value;
                        string datum = b.Attributes["datum"].Value;
                    }
                    if(id==help[0]&&nachname==help[1]&&vorname==help[2]&&svnr==help[3])
                        Console.WriteLine("Patient with ID: "+id+" has the same values as in the database");
                    else
                        Console.WriteLine("Patient with ID: " + id + " doesn't have the same values as in the database");
                
                patientcounter++;
            }
            if(patientcounter==patientlist.Length-1)
                Console.WriteLine("Same amount of patients in XML and database");
            else
                Console.WriteLine("Not the same amount of patients in XML and database");
        }
    }
}
