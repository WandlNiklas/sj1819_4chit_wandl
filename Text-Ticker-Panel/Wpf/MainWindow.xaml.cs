﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Text_Ticker_Panel;
using System.Windows.Threading;

namespace Wpf
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TextTicker tt;
        Canvas[,] parr;
        public MainWindow()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, EventArgs e)
        {
            grid1.Children.RemoveRange(0, 5);
            tt = new TextTicker(textBox1.Text);
            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Interval = new TimeSpan(0,0,0,0,Convert.ToInt32(textBox2.Text));
            timer1.Tick += Shift;

            parr = new Canvas[33, 7];

            for (int i = 0; i < 33; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    parr[i, j] = new Canvas()
                    {
                        Width = 50,
                        Height = 50,
                        Margin = new Thickness(50 * i, 50 * j, 0, 0),
                        Background = new SolidColorBrush(Colors.Red)
                    };
                    grid1.Children.Add(parr[i, j]);
                }
                timer1.Start();
            }
        }
        private void Shift(object sender, EventArgs e)
        {
            int help = tt.word.GetLength(0);
            if (tt.word.GetLength(0) > 33)
                help = 33;
            for (int i = 0; i < help; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (tt.word[i, j] == '█')
                        parr[i, j].Background = new SolidColorBrush(Colors.Blue);
                    else
                        parr[i, j].Background = new SolidColorBrush(Colors.Red);

                }
            }
            tt.ShiftLeft();
        }
    }
}
