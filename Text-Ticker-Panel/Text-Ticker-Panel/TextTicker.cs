﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Text_Ticker_Panel
{
    public class TextTicker
    {
        string text;
        public char[,] word;
        Dictionary<char, char[,]> letterlist = new Dictionary<char, char[,]>();
        public TextTicker(string s)
        {
            CreateLetters();
            int position = 0;
            this.text = s.ToUpper();
            int help = text.Length * 5;
            word = new char[help, 7];
            foreach (char c in text)
            {
                if (letterlist.ContainsKey(c))
                {
                    AddLettersToword(letterlist[c], position);
                    position += 5;
                }
            }
            //for (int i = 0; i < word.GetLength(0); i++)
            //{
            //    for (int j = 0; j < word.GetLength(1); j++)
            //    {
            //        Console.SetCursorPosition(i, j);
            //        Console.Write(word[i, j]);
            //    }
            //}
        }

        private void CreateLetters()
        {
            char[,] spacearr = new char[3, 1] { { ' ' }, { ' ' }, { ' ' }};
            letterlist.Add(' ', spacearr);
            char[,] iarr = new char[3, 5] { {' ',' ',  ' ',' ',' ' },
                                            { '█','█', '█', '█','█' },
                                            {' ',' ',  ' ',' ',' ' }};
            letterlist.Add('I', iarr);
            char[,] tarr = new char[3, 5] { {'█',' ',' ' ,' ', ' ' },
                                            {'█','█', '█','█','█', },
                                            {'█',' ',' ', ' ', ' ' }};
            letterlist.Add('T', tarr);
            char[,] larr = new char[3, 5] { {'█','█', '█','█', '█' },
                                            {' ',' ',' ' ,' ', '█' },
                                            {' ',' ',' ', ' ', '█' }};
            letterlist.Add('L', larr);
            char[,] oarr = new char[3, 5] { {'█','█', '█','█', '█' },
                                            {'█',' ',' ' ,' ', '█' },
                                            {'█','█', '█','█', '█' }};
            letterlist.Add('O', oarr);
            char[,] varr = new char[3, 5] { {'█','█', '█','█', ' ' },
                                            {' ',' ',' ' ,' ', '█' },
                                            {'█','█', '█','█', ' ' }};
            letterlist.Add('V', varr);
            char[,] earr = new char[3, 5] { {'█','█', '█', '█', '█' },
                                            {'█',' ', '█' ,' ', '█' },
                                            {'█',' ', '█', ' ', '█' }};
            letterlist.Add('E', earr);
        }
        private void AddLettersToword(char[,] l, int position)
        {
            for (int i = 0; i < l.GetLength(0); i++)
            {
                for (int j = 0; j < l.GetLength(1); j++)
                {
                    word[position +i, j] = l[i, j];
                }
            }
        }
        public void ShiftRight()
        {
            char[] shifter = new char[7];
            for (int i = 0; i < shifter.Length; i++)
                shifter[i] = word[word.GetLength(0) - 1, i];

            for (int i = word.GetLength(0) - 1; i > 0; i--)
            {
                for (int j = 0; j < word.GetLength(1); j++)
                {
                    word[i, j] = word[i - 1, j];
                }
            }

            for (int i = 0; i < shifter.Length; i++)
            {
                word[0, i] = shifter[i];
            }
        }
        public void ShiftLeft()
        {
            char[] shifter = new char[7];
            for (int i = 0; i < shifter.Length; i++)
                shifter[i] = word[0, i];

            for (int i = 0; i < word.GetLength(0) - 1; i++)
            {
                for (int j = 0; j < word.GetLength(1); j++)
                {
                    word[i, j] = word[i + 1, j];
                }
            }

            for (int i = 0; i < shifter.Length; i++)
            {
                word[word.GetLength(0) - 1, i] = shifter[i];
            }
        }
    }
}
