﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Text_Ticker_Panel
{
    class Program
    {
        static TextTicker tt;
        static void Main(string[] args)
        {
            Console.Write("Wort: ");
            string wort = Console.ReadLine();
            Console.Write("Ticks: ");
            int ticks = Convert.ToInt32(Console.ReadLine());
            tt = new TextTicker(wort);
            Timer t = new Timer(Shift, null, 0, ticks);
            Console.ReadLine();

        }
        static void Shift(object obj)
        {
            Console.Clear();
            int help = tt.word.GetLength(0);
            if (tt.word.GetLength(0) > 33)
                help = 33;
            for (int i = 0; i < help; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    Console.SetCursorPosition(i, j);
                    Console.Write(tt.word[i, j]);
                }
            }
            tt.ShiftRight();
        }
    }
}
