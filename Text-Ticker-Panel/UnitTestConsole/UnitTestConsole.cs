﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Text_Ticker_Panel;

namespace UnitTestConsole
{
    [TestClass]
    public class UnitTestConsole
    {
        [TestMethod]
        public void TestShift()
        {
            TextTicker tt = new TextTicker("i love it");
            char[,] expected = (char[,])tt.word.Clone();

            tt.ShiftLeft();
            tt.ShiftRight();

            CollectionAssert.AreEqual(expected, tt.word);
        }
    }
}
