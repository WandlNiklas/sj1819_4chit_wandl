﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Text_Ticker_Panel;

namespace Forms
{
    public partial class Form1 : Form
    {
        TextTicker tt;
        Panel[,] parr;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tt = new TextTicker(textBox1.Text);
            timer1.Interval = Convert.ToInt32(textBox2.Text);
            timer1.Tick += Shift;

            parr = new Panel[33,7];

            for(int i = 0; i < 33; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    parr[i, j] = new Panel()
                    {
                        Width = 50,
                        Height = 50,
                        Left = 50 * i,
                        Top = 50 * j,
                        BackColor = Color.Red
                    };
                    panel1.Controls.Add(parr[i, j]);
                }
            }

            timer1.Start();
        }
        private void Shift(object sender, EventArgs e)
        {
            int help = tt.word.GetLength(0);
            if (tt.word.GetLength(0) > 33)
                help = 33;
            for (int i = 0; i < help; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (tt.word[i,j] == '█')
                        parr[i, j].BackColor = Color.Blue;
                    else
                        parr[i, j].BackColor = Color.Red;

                }
            }
            tt.ShiftLeft(); 
        }
    }
}
