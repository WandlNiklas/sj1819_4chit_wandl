﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WPFDrawings
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static double end, end2;
        static List<Line> l = new List<Line>();
        static List<Ellipse> le = new List<Ellipse>();
        public MainWindow()
        {
            InitializeComponent();
            DispatcherTimer timer1 = new DispatcherTimer();
            Random randy = new Random();
            end = randy.Next((int)SystemParameters.PrimaryScreenWidth);
            end2 = randy.Next((int)SystemParameters.PrimaryScreenHeight);
            timer1.Interval = new TimeSpan(0, 0, 0, 0, 2000);
            timer1.Tick += DrawEllipses;
            timer1.Start();
        }
        private void DrawLines(object sender, EventArgs e)
        {
            Random rnd = new Random();
            SolidColorBrush randomColor = new SolidColorBrush();
            randomColor.Color = Color.FromArgb(255, (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            Line myLine = new Line();
            myLine.Stroke = randomColor;
            
            myLine.X1 = end;
            myLine.X2 = rnd.Next((int)SystemParameters.PrimaryScreenWidth);
            myLine.Y1 = end2;
            myLine.Y2 = rnd.Next((int)SystemParameters.PrimaryScreenHeight);
            end = myLine.X2;
            end2 = myLine.Y2;

            myLine.StrokeThickness = 10;
            
            if(l.Count<5)
            {
                l.Add(myLine);
                myGrid.Children.Add(myLine);
            }
            else
            {
                myGrid.Children.Remove(l[0]);
                l.RemoveAt(0);
                l.Add(myLine);
                myGrid.Children.Add(myLine);
            }  
        }

        private void DrawEllipses(object sender, EventArgs e)
        {
            Random rnd = new Random();
            SolidColorBrush randomColor = new SolidColorBrush();
            randomColor.Color = Color.FromArgb(255, (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            SolidColorBrush randomColor2 = new SolidColorBrush();
            randomColor2.Color = Color.FromArgb(255, (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            Ellipse myEllipse = new Ellipse();
            myEllipse.Fill = randomColor;
            
            myEllipse.StrokeThickness = 10;
            myEllipse.Stroke = randomColor2;

            myEllipse.Width = rnd.Next((int)SystemParameters.PrimaryScreenWidth);
            myEllipse.Height = rnd.Next((int)SystemParameters.PrimaryScreenHeight);

            double left = rnd.Next(-(int)SystemParameters.PrimaryScreenWidth,(int)SystemParameters.PrimaryScreenWidth);
            double top = rnd.Next(-(int)SystemParameters.PrimaryScreenHeight,(int)SystemParameters.PrimaryScreenHeight);

            myEllipse.Margin = new Thickness(left, top, 0, 0);

            if (le.Count < 5)
            {
                le.Add(myEllipse);
                myGrid.Children.Add(myEllipse);
            }
            else
            {
                myGrid.Children.Remove(le[0]);
                le.RemoveAt(0);
                le.Add(myEllipse);
                myGrid.Children.Add(myEllipse);
            }
        }
    }
}
