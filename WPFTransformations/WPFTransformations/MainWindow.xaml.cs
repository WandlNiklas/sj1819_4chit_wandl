﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFTransformations
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            test.RenderTransform = new RotateTransform(45);
            test.RenderTransform = new ScaleTransform(2, 1.5);
            test.RenderTransform = new TranslateTransform(100, 100);
            test.RenderTransform = new SkewTransform(10, 3);
        }
    }
}
