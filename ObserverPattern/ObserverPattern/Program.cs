﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Pegelstand?: ");
            int ps = Convert.ToInt32(Console.ReadLine());
            Kienstock k = new Kienstock();
            k.GetwasserStand = ps;
            Schifffahrtsbehörde sb = new Schifffahrtsbehörde(k, "Schifffahrtsbehörde", 3);
            Katastrophenschutz ks = new Katastrophenschutz(k, "Katastrophenschutz", 9);
            Landeswarnzentrale lw = new Landeswarnzentrale(k, "Landeswarnzentrale", 7);
            k.Attach(sb);
            k.Attach(ks);
            k.Attach(lw);
            k.Notify();

            Console.WriteLine();

            Kienstock k2 = new Kienstock();
            k2.GetwasserStand = ps;
            Schifffahrtsbehörde sb2 = new Schifffahrtsbehörde(k2, "Schifffahrtsbehörde_Event", 3);
            Katastrophenschutz ks2 = new Katastrophenschutz(k2, "Katastrophenschutz_Event", 9);
            Landeswarnzentrale lw2 = new Landeswarnzentrale(k2, "Landeswarnzentrale_Event", 7);
            k2.CallUpdate();
        }
    }
}
