﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class Landeswarnzentrale:IObserver
    {
        private int wasserStand;
        private int _oberGrenze;
        private string _name;
        private Kienstock _k;

        public Landeswarnzentrale(Kienstock k, string name, int oberGrenze)
        {
            this._k = k;
            this._name = name;
            this._oberGrenze = oberGrenze;
            _k.Ws += K_Ws;
        }

        private void K_Ws()
        {
            wasserStand = _k.GetwasserStand;
            if (wasserStand > _oberGrenze)
            {
                Console.WriteLine(_name + ": Wasserstand: " + wasserStand + "; max. Wasserstand: " + _oberGrenze + "; --> Gefahr!");
            }
            else
                Console.WriteLine(_name + ": Wasserstand: " + wasserStand + "; max. Wasserstand: " + _oberGrenze + "; --> Status okay!");
        }

        public void Update()
        {
            wasserStand = _k.GetwasserStand;
            if (wasserStand > _oberGrenze)
            {
                Console.WriteLine(_name + ": Wasserstand: "+wasserStand+"; max. Wasserstand: "+_oberGrenze+"; --> Gefahr!");
            }
            else
                Console.WriteLine(_name + ": Wasserstand: " + wasserStand + "; max. Wasserstand: " + _oberGrenze + "; --> Status okay!");
        }
    }
}
