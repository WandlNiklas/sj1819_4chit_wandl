﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObserverPattern
{
    class Kienstock:ISubject
    {
        private int _wasserStand;

        public delegate void WasserStand();
        public event WasserStand Ws;

        public int GetwasserStand
        {
            get { return _wasserStand; }
            set { _wasserStand = value; }
        }
        List<IObserver> observers = new List<IObserver>();

        public void Attach(IObserver observer)
        {
            observers.Add(observer);
        }

        public void Detach(IObserver observer)
        {
            observers.Remove(observer);
        }

        public void Notify()
        {
            foreach (IObserver o in observers)
            {
                o.Update();
            }
        }
        public void CallUpdate()
        {
            Ws();
        }
    }
}
