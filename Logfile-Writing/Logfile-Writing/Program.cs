﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace Logfile_Writing
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(DoSomething);
            t1.Name = "David";
            t1.Start(1);
            t1.Join();
            Thread t2 = new Thread(DoSomething);
            t2.Name = "Kitzler";
            t2.Start(2);
            t2.Join();
            Thread t3 = new Thread(DoSomething);
            t3.Name = "und";
            t3.Start(3);
            t3.Join();
            Thread t4 = new Thread(DoSomething);
            t4.Name = "sein";
            t4.Start(4);
            t4.Join();
            Thread t5 = new Thread(DoSomething);
            t5.Name = "Lieblingsfach";
            t5.Start(5);
            t5.Join();
        }
        static void WriteLog(string message)
        {
            StreamReader sr = new StreamReader("logfilecounter.txt");
            int counter = Convert.ToInt32(sr.ReadLine());
            sr.Close();

            StreamReader sr2 = new StreamReader("alllogs_" + counter + ".txt");
            string[]h = sr2.ReadToEnd().Split('\n');
            sr2.Close();
            int lines = h.Length;

            if (lines > 10)
            {
                counter++;
                StreamWriter sw2 = new StreamWriter("logfilecounter.txt");
                sw2.WriteLine(counter);
                sw2.Flush();
                sw2.Close();
            }

            StreamWriter sw = new StreamWriter("alllogs_"+counter+".txt", true);
            sw.WriteLine(DateTime.Now + ":"+DateTime.Now.Millisecond + " " + message);
            sw.Flush();
            sw.Close();

        }
        static void DoSomething(object obj)
        {
            object locker = new Object();
            //Monitor.Enter(locker);
            lock (locker)
            {
                int n = (int)obj;
                Thread.Sleep(n * 500);
                WriteLog(Thread.CurrentThread.Name+" Thread " + n + " - where " + n + " is the actual number of the thread");
            }
            //Monitor.Exit(locker);
        }
    }
}
