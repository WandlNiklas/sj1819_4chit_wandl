﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Configuration;

namespace MiniWebserver
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients
        static void Main(string[] args)
        {
            listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8888);
            listener.Start();
            #if LOG
            Console.WriteLine("Server mounted, 
                            listening to port 8888");
            #endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }
        public static void Service()
        {
            string repo = @"C:\Users\Niklas\Desktop\Schule\JAHR 4\SEW\Projekte\190605\MiniWebserver\files\";
            string defaultresponse = "HTTP/1.1 200 OK\nDate: "+DateTime.Now+ "\nServer: localhost\nContent-Type: text/html\nContent-Length: 999\nConnection: Closed\n";

            while (false == false)
            {
                string defaultreponse1 = "HTTP/1.1 200 OK";
                string defaultreponse2 = "Date: " + DateTime.Now;
                string defaultreponse3 = "Server: localhost";
                string defaultreponse4 = "Content-Type: text/html";
                Socket soc = listener.AcceptSocket();
#if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);
#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true; // enable automatic flushing
                    string data = sr.ReadLine().ToString().Split(' ')[1].Trim('/');
                    if (data.Substring(data.Length - 4) == ".png")
                    {
                        if (File.Exists(repo + data))
                        {
                            defaultreponse4 = "Content-Type: image/png";
                            sw.WriteLine(defaultreponse1);
                            sw.WriteLine(defaultreponse2);
                            sw.WriteLine(defaultreponse3);
                            sw.WriteLine(defaultreponse4);
                            sw.WriteLine("");
                            soc.SendFile(repo + data);
                            Thread.Sleep(200);
                        }
                        else
                        {
                            sw.Write(File.ReadAllText(repo + "notfound.html"));
                            Thread.Sleep(200);
                        }
                    }
                    if (data.Substring(data.Length - 4) == ".jpg")
                    {
                        if (File.Exists(repo + data))
                        {
                            defaultreponse4 = "Content-Type: image/jpg";
                            sw.WriteLine(defaultreponse1);
                            sw.WriteLine(defaultreponse2);
                            sw.WriteLine(defaultreponse3);
                            sw.WriteLine(defaultreponse4);
                            sw.WriteLine("");
                            soc.SendFile(repo + data);
                            Thread.Sleep(200);
                        }
                        else
                        {
                            sw.Write(File.ReadAllText(repo + "notfound.html"));
                            Thread.Sleep(200);
                        }
                    }
                    if (data.Substring(data.Length - 4) == ".gif")
                    {
                        if (File.Exists(repo + data))
                        {
                            defaultreponse4 = "Content-Type: image/gif";
                            sw.WriteLine(defaultreponse1);
                            sw.WriteLine(defaultreponse2);
                            sw.WriteLine(defaultreponse3);
                            sw.WriteLine(defaultreponse4);
                            sw.WriteLine("");
                            soc.SendFile(repo + data);
                            Thread.Sleep(200);
                        }
                        else
                        {
                            sw.Write(File.ReadAllText(repo + "notfound.html"));
                            Thread.Sleep(200);
                        }
                    }
                    if (data.Substring(data.Length - 5) == ".html")
                    {
                        if (File.Exists(repo + data))
                        {
                            sw.WriteLine(defaultreponse1);
                            sw.WriteLine(defaultreponse2);
                            sw.WriteLine(defaultreponse3);
                            sw.WriteLine(defaultreponse4);
                            sw.WriteLine("");
                            sw.Write(File.ReadAllText(repo + data));
                            Thread.Sleep(200);
                        }
                        else if (data == "")
                        {
                            sw.WriteLine(defaultreponse1);
                            sw.WriteLine(defaultreponse2);
                            sw.WriteLine(defaultreponse3);
                            sw.WriteLine(defaultreponse4);
                            sw.WriteLine("");
                            sw.Write(File.ReadAllText(repo + "index.html"));
                            Thread.Sleep(200);
                        }
                        else
                        {
                            sw.Write(File.ReadAllText(repo + "notfound.html"));
                            Thread.Sleep(200);
                        }
                    }
                    else
                    {
                        sw.Write(File.ReadAllText(repo + "notallowed.html"));
                        Thread.Sleep(200);
                    }

                    s.Close();
                }
                catch (Exception e)
                {
#if LOG
                        Console.WriteLine(e.Message);
#endif
                }
                #if LOG
                    Console.WriteLine("Disconnected: {0}", 
                                            soc.RemoteEndPoint);
                #endif
                soc.Close();
            }
        }
    }
}
