﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;
using System.IO.Compression;

namespace Archiver
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult dr = fbd.ShowDialog();
            string folder = fbd.SelectedPath;
            if (dr == DialogResult.OK)
            {
                backgroundWorker1.RunWorkerAsync(folder);
            }
        }
        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            DirectoryInfo di = new DirectoryInfo(@e.Argument.ToString());
            FileInfo[] fi = di.GetFiles("*.txt");
            StreamWriter sw = new StreamWriter("archive.arc");
            int counter = 0;
            foreach (FileInfo file in fi)
            {
                StreamReader sr = new StreamReader(@e.Argument.ToString()+"\\"+file.ToString());
                string result = sr.ReadToEnd();
                sw.Write(result);
                sw.WriteLine(Environment.NewLine + "/*_*/ "+file.Name);
                counter++;
                backgroundWorker1.ReportProgress(counter *100/fi.Length);
            }
            e.Result = "Archived " + fi.Length + " Files!";
            sw.Flush();
            sw.Close();
            if (File.Exists("archive.arc.zip"))
                File.Delete("archive.arc.zip");
            using (ZipArchive zip = ZipFile.Open("archive.arc.zip", ZipArchiveMode.Create))
            {
                zip.CreateEntryFromFile("archive.arc", "archive.arc");
            }
            File.Delete("archive.arc");
        }
        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }
        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(e.Result.ToString());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = 100;
            progressBar1.Step = 1;
            progressBar1.Value = 0;
            string file = "archive.arc.zip";
            if (File.Exists(@"unzipped\archive.arc"))
                File.Delete(@"unzipped\archive.arc");
            ZipFile.ExtractToDirectory(file,"unzipped");
            file = @"unzipped\archive.arc";
            backgroundWorker2.RunWorkerAsync(file);
        }
        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            StreamReader sr = new StreamReader(e.Argument.ToString());
            StreamReader sr2 = new StreamReader(e.Argument.ToString());
            string s = sr2.ReadToEnd();
            s = s.Replace("\r\n", " ");
            string content = "";
            string line;
            int counter = 0;
            int count = Regex.Matches(s, @"\b/*_*/\b").Count;
            while (sr.Peek() != -1)
            {
                line = sr.ReadLine();
                if (line.IndexOf("/*_*/") != -1)
                {
                    counter++;
                    string[] help = line.Split(' ');
                    string name = "";
                    for (int i = 1; i < help.Length; i++)
                    {
                        name += help[i]+" ";
                    }
                    StreamWriter sw = new StreamWriter(name);
                    sw.Write(content);
                    sw.Flush();
                    sw.Close();
                }
                else
                {
                    content += line + Environment.NewLine;
                }
                backgroundWorker2.ReportProgress(counter * 100 / count);
            }
            e.Result = "Dearchived " + count + " Files!";
        }
        private void backgroundWorker2_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }
        private void backgroundWorker2_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            MessageBox.Show(e.Result.ToString());
        }
    }
}

