﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    abstract class SelectedDecorator:Shape
    {
        public Shape S { get; set; }
        public void SetShape(Shape s)
        {
            this.S = s;
        }
        public override void Operation()
        {
            S.Operation();
        }
        public override bool amIselected(double m_x, double m_y)
        {
            return false;
        }
        public SelectedDecorator(Grid g, int x, int y,int thickness, SolidColorBrush c) : base(g, x, y, thickness, c) { }
    }
}
