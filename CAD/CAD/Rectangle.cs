﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    public class Rectangle : Shape
    {
        public Rectangle(int startx, int starty, int thickness, SolidColorBrush color, int height,int width) : base(startx, starty, thickness, color)
        {
            Height = height;
            Width = width;
        }
        public int Width { get; set; }

        public int Height { get; set; }
        public override void Draw(Grid myGrid)
        {
            //Random rnd = new Random();
            //Line myLine = new Line();
            //myLine.Stroke = Color;

            //myLine.X1 = StartX + myGrid.ActualWidth * 0.25;
            //myLine.Y1 = StartY;
            //myLine.X2 = EndX + myGrid.ActualWidth * 0.25;
            //myLine.Y2 = EndY;

            //myLine.StrokeThickness = Thickness;
            //myGrid.Children.Add(myLine);
        }
    }
}