﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace CAD
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Shape> shapelist = new List<Shape>();
        Line l = new Line();
        Random rnd;
        int counter;
        public static MainWindow AppWindow;
        public MainWindow()
        {
            InitializeComponent();
            rnd = new Random();
            DispatcherTimer timer1 = new DispatcherTimer();
            timer1.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timer1.Tick += Timer1_Tick;
            timer1.Start();
            myGrid.Children.Remove(redrawButton);
            myGrid.Children.Add(l);
            AppWindow = this;
            cb.Items.Add("Line");
            cb.Items.Add("Rectangle");
            cb.Items.Add("Circle");

            cb.SelectedIndex = 0;
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            myGrid.Children.Remove(l);
            l = new Line();
            l.X1 = Width * 0.25;
            l.X2 = Width * 0.25;
            l.Y1 = 0;
            l.Y2 = Height;
            l.StrokeThickness = 10;
            l.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            myGrid.Children.Add(l);
        }

        public void button1_Click(object sender, RoutedEventArgs e)
        {
            if (!myGrid.Children.Contains(ly2))
            {
                myGrid.Children.Add(ly2);
            }
            if (!myGrid.Children.Contains(ty2))
            {
                myGrid.Children.Add(ty2);
            }
            if ((string)cb.SelectedItem == "Line")
            {
                lDraw.Content = "Draw a line:";
                lx1.Content = "X1:";
                ly1.Content = "Y1:";
                lx2.Content = "X2:";
                ly2.Content = "Y2:";
            }
            if ((string)cb.SelectedItem == "Rectangle")
            {
                lDraw.Content = "Draw a rectangle:";
                lx1.Content = "Left:";
                ly1.Content = "Top:";
                lx2.Content = "Height:";
                ly2.Content = "Width:";
            }
            if ((string)cb.SelectedItem == "Circle")
            {
                lDraw.Content = "Draw a circle:";
                lx1.Content = "Center1:";
                ly1.Content = "Center2:";
                lx2.Content = "Radius:";
                myGrid.Children.Remove(ly2);
                myGrid.Children.Remove(ty2);
            }
        }
        public void Button_Click(object sender, RoutedEventArgs e)
        {
            SolidColorBrush randomColor = new SolidColorBrush();
            randomColor.Color = Color.FromArgb(255, (byte)rnd.Next(256), (byte)rnd.Next(256), (byte)rnd.Next(256));
            int[] values = new int[4];
            try
            {
                values[0] = Convert.ToInt32(tx1.Text);
                values[1] = Convert.ToInt32(ty1.Text);
                values[2] = Convert.ToInt32(tx2.Text);

                if (cb.SelectedIndex != 2)
                {
                    values[3] = Convert.ToInt32(ty2.Text);
                }
            }
            catch
            {
                MessageBox.Show("Nur Ganzzahlen erlaubt!");
            }
            if (cb.SelectedIndex == 1)
            {
                try
                {
                    if (Convert.ToInt32(tx2.Text) < 0 || Convert.ToInt32(ty2.Text) < 0)
                    {
                        MessageBox.Show("Nur positive Werte für Width/Height!");
                    }
                }
                catch
                {
                    MessageBox.Show("Nur positive Werte für Width/Height!");
                }
            }
            if (cb.SelectedIndex == 2 && Convert.ToInt32(tx2.Text) < 0)
            {
                MessageBox.Show("Nur positive Werte für Radius!");
            }
            if ((string)cb.SelectedItem == "Line")
            {
                Linee l = new Linee(myGrid, values[0], values[1], 10, randomColor, values[2], values[3]);
                myGrid.Children.Add(l.Draw());
                shapelist.Add(l);
            }
            if ((string)cb.SelectedItem == "Rectangle")
            {
                Rectanglee r = new Rectanglee(myGrid, values[0], values[1], 10, randomColor, values[2], values[3]);
                
                myGrid.Children.Add(r.Draw());
                shapelist.Add(r);
            }
            if ((string)cb.SelectedItem == "Circle")
            {
                Circle c = new Circle(myGrid, values[0], values[1], 10, randomColor, values[2]);
                myGrid.Children.Add(c.Draw());
                shapelist.Add(c);
            }
            tx1.Text = "";
            ty1.Text = "";
            tx2.Text = "";
            ty2.Text = "";
        }

        private void MouseLeftButtonDown_Click(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(AppWindow);
            double m_x = p.X;
            double m_y = p.Y;
            
            foreach (Shape s in shapelist)
            {
                if (s.amIselected(m_x, m_y))
                {
                    counter = shapelist.IndexOf(s);
                    myGrid.Children.Remove(drawButton);
                    if (!myGrid.Children.Contains(redrawButton))
                    {
                        myGrid.Children.Add(redrawButton);
                    }
                    if (!myGrid.Children.Contains(ly2))
                    {
                        myGrid.Children.Add(ly2);
                    }
                    if(!myGrid.Children.Contains(ty2))
                    {
                        myGrid.Children.Add(ty2);
                    }
                    if (shapelist[counter] is Linee)
                    {
                        Linee l = (Linee)shapelist[counter];
                        tx1.Text = l.StartX.ToString();
                        ty1.Text = l.StartY.ToString();
                        tx2.Text = l.EndX.ToString();
                        ty2.Text = l.EndY.ToString();
                        lDraw.Content = "Redraw a line:";
                        lx1.Content = "X1:";
                        ly1.Content = "Y1:";
                        lx2.Content = "X2:";
                        ly2.Content = "Y2:";
                        LineSelectedDecorator d1 = new LineSelectedDecorator(helpGrid, l.StartX, l.StartY, 10, l.Color);
                        d1.S = l;
                        d1.SetShape(l);
                        d1.Operation();
                    }
                    if (shapelist[counter] is Rectanglee)
                    {
                        Rectanglee r = (Rectanglee)shapelist[counter];
                        tx1.Text = r.StartX.ToString();
                        ty1.Text = r.StartY.ToString();
                        tx2.Text = r.Height.ToString();
                        ty2.Text = r.Width.ToString();
                        lDraw.Content = "Redraw a rectangle:";
                        lx1.Content = "Left:";
                        ly1.Content = "Top:";
                        lx2.Content = "Height:";
                        ly2.Content = "Width:";
                        RectangleSelectedDecorator d1 = new RectangleSelectedDecorator(helpGrid, r.StartX, r.StartY, 10, r.Color);
                        d1.S = r;
                        d1.SetShape(r);
                        d1.Operation();
                    }
                    if (shapelist[counter] is Circle)
                    {
                        Circle c = (Circle)shapelist[counter];
                        tx1.Text = c.StartX.ToString();
                        ty1.Text = c.StartY.ToString();
                        tx2.Text = c.Radius.ToString();
                        lDraw.Content = "Redraw the circle:";
                        lx1.Content = "Center1:";
                        ly1.Content = "Center2:";
                        lx2.Content = "Radius:";
                        myGrid.Children.Remove(ly2);
                        myGrid.Children.Remove(ty2);
                        CircleSelectedDecorator d1 = new CircleSelectedDecorator(helpGrid, c.StartX, c.StartY, 10, c.Color);
                        d1.S = c;
                        d1.SetShape(c);
                        d1.Operation();
                    }
                    break;
                }
            }
        }

        private void B_Click(object sender, RoutedEventArgs e)
        {
            helpGrid.Children.RemoveRange(1, 99);
            if (shapelist[counter] is Linee)
            {
                Linee l = (Linee)shapelist[counter];
                l.StartX = Convert.ToInt32(tx1.Text);
                l.StartY = Convert.ToInt32(ty1.Text);
                l.EndX = Convert.ToInt32(tx2.Text);
                l.EndY = Convert.ToInt32(ty2.Text);
                myGrid.Children.RemoveAt(counter + 11);
                myGrid.Children.Add(l.Draw());
            }
            else if (shapelist[counter] is Rectanglee)
            {
                Rectanglee r = (Rectanglee)shapelist[counter];
                r.StartX = Convert.ToInt32(tx1.Text);
                r.StartY = Convert.ToInt32(ty1.Text);
                r.Height = Convert.ToInt32(tx2.Text);
                r.Width = Convert.ToInt32(ty2.Text);
                myGrid.Children.RemoveAt(counter + 11);
                myGrid.Children.Add(r.Draw());
            }
            else if (shapelist[counter] is Circle)
            {
                Circle c = (Circle)shapelist[counter];
                c.StartX = Convert.ToInt32(tx1.Text);
                c.StartY = Convert.ToInt32(ty1.Text);
                c.Radius = Convert.ToInt32(tx2.Text);
                myGrid.Children.RemoveAt(counter + 9);
                myGrid.Children.Add(c.Draw());
            }
            myGrid.Children.Add(drawButton);
            myGrid.Children.Remove(redrawButton);

            tx1.Text = "";
            ty1.Text = "";
            tx2.Text = "";
            ty2.Text = "";
        }
    }
}

