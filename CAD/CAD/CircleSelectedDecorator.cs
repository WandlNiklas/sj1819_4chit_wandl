﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    class CircleSelectedDecorator : SelectedDecorator
    {
        public CircleSelectedDecorator(Grid g, int x, int y, int thickness, SolidColorBrush c) : base(g, x, y, thickness, c) { }

        public override void Operation()
        {
            SolidColorBrush scb = new SolidColorBrush();
            scb.Color = Colors.Gray;
            base.Operation();
            Circle help = (Circle)S;
            Rectangle r1 = new Rectangle();
            r1.Width = 10;
            r1.Height = 10;
            r1.Fill = scb;
            r1.StrokeThickness = 1;
            r1.Margin = new Thickness(help.StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth - help.Radius*2, help.StartY - MainWindow.AppWindow.myGrid.ActualHeight, 0, 0);
            MainWindow.AppWindow.helpGrid.Children.Add(r1);
            Rectangle r2 = new Rectangle();
            r2.Width = 10;
            r2.Height = 10;
            r2.Fill = scb;
            r2.StrokeThickness = 1;
            r2.Margin = new Thickness(help.StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth, help.StartY - MainWindow.AppWindow.myGrid.ActualHeight - help.Radius*2, 0, 0);
            MainWindow.AppWindow.helpGrid.Children.Add(r2);
            Rectangle r3 = new Rectangle();
            r3.Width = 10;
            r3.Height = 10;
            r3.Fill = scb;
            r3.StrokeThickness = 1;
            r3.Margin = new Thickness(help.StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth + help.Radius*2, help.StartY - MainWindow.AppWindow.myGrid.ActualHeight, 0, 0);
            MainWindow.AppWindow.helpGrid.Children.Add(r3);
            Rectangle r4 = new Rectangle();
            r4.Width = 10;
            r4.Height = 10;
            r4.Fill = scb;
            r4.StrokeThickness = 1;
            r4.Margin = new Thickness(help.StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth, help.StartY - MainWindow.AppWindow.myGrid.ActualHeight + help.Radius*2, 0, 0);
            MainWindow.AppWindow.helpGrid.Children.Add(r4);
        }
    }
}
