﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    public class Linee : Shape
    {
        public Linee(Grid myGrid, int startx, int starty, int thickness, SolidColorBrush color, int endx, int endy) : base(myGrid, startx, starty, thickness, color)
        {
            StartX = startx;
            StartY = starty;
            Thickness = thickness;
            Color = color;
            EndX = endx;
            EndY = endy;
        }
        public override void Operation()
        {
            this.Draw();
        }
        public Line Draw()
        {
            Line myLine = new Line();
            myLine.Stroke = Color;

            myLine.X1 = StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.25;
            myLine.Y1 = StartY;
            myLine.X2 = EndX + MainWindow.AppWindow.myGrid.ActualWidth * 0.25;
            myLine.Y2 = EndY;

            myLine.StrokeThickness = Thickness;
            return myLine;
        }
        public int EndX { get; set; }

        public int EndY { get; set; }

        public override bool amIselected(double m_x, double m_y)
        {
            if (m_x > StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth && m_x < StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 + (EndX-StartX))
            {
                if (m_y > StartY - MainWindow.AppWindow.myGrid.ActualHeight && m_y < StartY + (EndY-StartY))
                    return true;
            }
            return false;
        }
    }
}