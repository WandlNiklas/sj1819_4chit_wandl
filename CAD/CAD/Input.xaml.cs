﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace CAD
{
    /// <summary>
    /// Interaktionslogik für Input.xaml
    /// </summary>
    public partial class Input : Window
    {
        public int[] values = new int[4];
        public Input()
        {
            InitializeComponent();

        }
        public void Button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                values[0] = Convert.ToInt32(tx1.Text);
                values[1] = Convert.ToInt32(ty1.Text);
                values[2] = Convert.ToInt32(tx2.Text);

                if (MainWindow.AppWindow.cb.SelectedIndex != 2)
                {
                    values[3] = Convert.ToInt32(ty2.Text);
                }

            }
            catch
            {
                MessageBox.Show("Nur Ganzzahlen erlaubt!");
            }
            if (MainWindow.AppWindow.cb.SelectedIndex == 1)
            {
                if (Convert.ToInt32(tx2.Text) < 0 || Convert.ToInt32(ty2.Text) < 0)
                {
                    MessageBox.Show("Nur positive Werte für Width/Height!");
                    return;
                }
            }
            if (MainWindow.AppWindow.cb.SelectedIndex == 2 && Convert.ToInt32(tx2.Text) < 0)
            {
                MessageBox.Show("Nur positive Werte für Radius!");
                return;
            }
            //MainWindow.AppWindow.button1_Click(sender, e);
            values[0] = 0;
            values[1] = 0;
            values[2] = 0;
            values[3] = 0;
            tx1.Text = "";
            ty1.Text = "";
            tx2.Text = "";
            ty2.Text = "";
            this.Close();
        }
    }
}
