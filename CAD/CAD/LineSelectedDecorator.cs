﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    class LineSelectedDecorator : SelectedDecorator
    {
        public LineSelectedDecorator(Grid g, int x, int y, int thickness, SolidColorBrush c) : base(g, x, y, thickness, c) { }

        public override void Operation()
        {
            SolidColorBrush scb = new SolidColorBrush();
            scb.Color = Colors.Gray;
            base.Operation();
            Linee help = (Linee)S;
            Rectangle r1 = new Rectangle();
            r1.Width = 10;
            r1.Height = 10;
            r1.Fill = scb;
            r1.StrokeThickness = 1;
            r1.Margin = new Thickness(help.StartX+MainWindow.AppWindow.myGrid.ActualWidth*0.25-5, help.StartY-5, 0, 0);
            r1.HorizontalAlignment = HorizontalAlignment.Left;
            r1.VerticalAlignment = VerticalAlignment.Top;
            MainWindow.AppWindow.helpGrid.Children.Add(r1);
            Rectangle r2 = new Rectangle();
            r2.Width = 10;
            r2.Height = 10;
            r2.Fill = scb;
            r2.StrokeThickness = 1;
            r2.HorizontalAlignment = HorizontalAlignment.Left;
            r2.VerticalAlignment = VerticalAlignment.Top;
            r2.Margin = new Thickness(help.EndX + MainWindow.AppWindow.myGrid.ActualWidth * 0.25-5, help.EndY-5, 0, 0);
            MainWindow.AppWindow.helpGrid.Children.Add(r2);
        }
    }
}
