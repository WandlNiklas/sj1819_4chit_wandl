﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    public class Circle : Shape
    {
        public Circle(Grid myGrid, int startx, int starty, int thickness, SolidColorBrush color, int radius) : base(myGrid,startx, starty, thickness, color)
        {
            Radius = radius;
        }
        public override void Operation()
        {
            this.Draw();
        }
        public Ellipse Draw()
        {
            Ellipse myEllipse = new Ellipse();
            myEllipse.Fill = Color;

            myEllipse.StrokeThickness = Thickness;
            myEllipse.Stroke = Color;

            myEllipse.Width = Radius * 2;
            myEllipse.Height = Radius * 2;

            myEllipse.Margin = new Thickness(StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth, StartY - MainWindow.AppWindow.myGrid.ActualHeight, 0, 0);
            return myEllipse;
        }
        public int Radius { get; set; }
        public override bool amIselected(double m_x, double m_y)
        {
            if (m_x > StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth && m_x < StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 + Radius*2)
            {
                if (m_y > StartY - MainWindow.AppWindow.myGrid.ActualHeight && m_y < StartY + Radius*2)
                    return true;
            }
            return false;
        }
    }
}