﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    public class Rectanglee : Shape
    {
        
        public Rectanglee(Grid myGrid,int startx, int starty, int thickness, SolidColorBrush color, int height,int width) : base(myGrid,startx, starty, thickness, color)
        {
            Height = height;
            Width = width;
        }
        public override void Operation()
        {
            this.Draw();
        }
        public Rectangle Draw()
        {
            Rectangle myRectangle = new Rectangle();
            myRectangle.Fill = Color;

            myRectangle.StrokeThickness = Thickness;
            myRectangle.Stroke = Color;

            myRectangle.Width = Width;
            myRectangle.Height = Height;

            myRectangle.Margin = new Thickness(StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth, StartY - MainWindow.AppWindow.myGrid.ActualHeight, 0, 0);
            return myRectangle;
        }
        public int Width { get; set; }

        public int Height { get; set; }

        public override bool amIselected(double m_x, double m_y)
        {
            if (m_x > StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 - MainWindow.AppWindow.myGrid.ActualWidth && m_x < StartX + MainWindow.AppWindow.myGrid.ActualWidth * 0.5 + Width)
            {
                if (m_y > StartY - MainWindow.AppWindow.myGrid.ActualHeight  && m_y < StartY + Height)
                    return true;
            }
            return false;
        }
    }
}