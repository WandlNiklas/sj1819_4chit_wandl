﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CAD
{
    abstract public class Shape
    {
        public Shape(Grid g, int startx, int starty, int thickness, SolidColorBrush color)
        {
            StartX = startx;
            StartY = starty;
            Thickness = thickness;
            Color = color;
        }
        public int StartX { get; set; }

        public int StartY { get; set; }

        public int Counter { get; set; }

        public int Thickness { get; set; }

        public Grid G { get; set; }
        public abstract void Operation();
        public SolidColorBrush Color{ get; set; }

        public abstract bool amIselected(double m_x, double m_y);
         
    }
}