﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AsyncHTTPRequest
{
    class Program
    {
        static void Main(string[] args)
        {
            string ws = "https://orf.at";
            CountTags(ws).Wait();
            Console.WriteLine("Img-Tags on orf.at: " + CountTags(ws).Result);
        }
        static async Task<int> CountTags(string s)
        {
            Task<int> t = Task.Factory.StartNew(() =>
            {
               WebClient client = new WebClient();
               string htmlCode = client.DownloadString(s);
               Console.WriteLine(htmlCode);
               int count = Regex.Matches(htmlCode, "<img").Count;
               return count;
            });
            await t;
            return t.Result;
        }
    }
}
