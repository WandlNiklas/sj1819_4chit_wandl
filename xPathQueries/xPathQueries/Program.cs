﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace xPathQueries
{
    class Program
    {
        static void Main(string[] args)
        {
            TaskAa();
            TaskAb();
            TaskAc();
            TaskAd();
            TaskAe();
        }
        static void TaskAa()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Result.tea");
            int counter = 0;
            foreach (XmlNode row in doc.SelectNodes("//Pupil[Punkte>30]"))
            {
                counter++;
            }
            Console.WriteLine(counter+" students have more than 30 Points!");
        }
        static void TaskAb()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Result.tea");
            XmlNode n = doc.SelectSingleNode("//Pupil[@ID='941']");
            string vname = n.SelectSingleNode("Vorname").InnerText;
            string nname = n.SelectSingleNode("Nachname").InnerText;
            Console.WriteLine(vname+" " + nname + " is the name of pupil 941!");
        }
        static void TaskAc()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Result.tea");
            double counter = 0;
            foreach (XmlNode row in doc.SelectNodes("//Pupil"))
            {
                if(row.SelectSingleNode("Punkte").InnerText!="-")
                counter += Convert.ToDouble(row.SelectSingleNode("Punkte").InnerText);
            }
            XmlNode n = doc.SelectSingleNode("TestResult");
            double testpoints = Convert.ToDouble(n.Attributes["SumPunkte"].Value);
            Console.WriteLine(counter + " is the sum of all points! Testresult points are: "+testpoints);
        }
        static void TaskAd()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Result.tea");
            double counter = 0;
            int counter_ = 0;
            foreach (XmlNode row in doc.SelectNodes("//Pupil"))
            {
                if (row.Attributes["Importdatum"].InnerText != "--,--,----")
                {
                    counter += Convert.ToDouble(row.SelectSingleNode("Punkte").InnerText);
                    counter_++;
                }
            }
            Console.WriteLine(counter/counter_ + " are the average points of the students, who attended the test!");
        }
        static void TaskAe()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Result.tea");
            Console.WriteLine();
            Console.WriteLine("KatNr\tNachname\tVorname\tPunkte\tProzent\tBewertung\tNote");
            foreach (XmlNode p in doc.SelectNodes("//Pupil"))
            {
                string kat = p.SelectSingleNode("KatNr").InnerText;
                string nachname = p.SelectSingleNode("Nachname").InnerText;
                string vorname = p.SelectSingleNode("Vorname").InnerText;
                string punkte = p.SelectSingleNode("Punkte").InnerText;
                string prozent = p.SelectSingleNode("Prozent").InnerText;
                string bewertung = p.SelectSingleNode("Bewertung").InnerText;
                string note = p.SelectSingleNode("Note").InnerText;
                Console.WriteLine(kat+ "\t" + nachname+ "\t" + vorname+ "\t" + punkte+ "\t" + prozent+ "\t" + bewertung+ "\t" + note);
            }
        }
    }
}
