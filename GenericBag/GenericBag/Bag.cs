﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBag
{
    class Bag<T> where T : IDescription
    {
        List<T> items;
        int size;
        public Bag(int size)
        {
            this.size = size;
            items = new List<T>(size);
        }

        public void putItem(T item)
        {
            if(items.Count>=size)
                Console.WriteLine("Bag full - no more items can be added!");
            else
            {
                foreach(T t in items)
                {
                    if(t.Description==item.Description&&t.Size==item.Size)
                    {
                        Console.WriteLine("Item not added, because an item with the same Size and Description already exists!");
                        return;
                    }
                }
                items.Add(item);
                Console.WriteLine("Item added successfully!");
            }
        }

        public T getItem()
        {
            if (items.Count > 0)
            {
                T t = Weighting();
                items.Remove(Weighting());
                return t;
            }
            else
            {
                Console.WriteLine("Bag empty!");
                return default(T);
            }
        }

        public string Content()
        {
            StringBuilder sb = new StringBuilder("Items: ");
            foreach (T t in items)
                sb.Append(t.ToString() + ";");
            return sb.ToString();
        }

        public T Weighting()
        {
            T heaviest = default(T);
            int sumAsciiMax = 0;
            foreach(T t in items)
            {
                int sumAscii = 0;
                byte[] ascii = Encoding.ASCII.GetBytes(t.Description);
                foreach (Byte b in ascii)
                {
                    sumAscii += b;
                }
                if (sumAscii*t.Size > sumAsciiMax) //if same Weight, the first item, that was in the bag, gets chosen
                {
                    sumAsciiMax = sumAscii*t.Size;
                    heaviest = t;
                }
            }
            return heaviest;
        }
    }
}
