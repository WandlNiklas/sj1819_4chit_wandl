﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBag
{
    class Item:IDescription
    {
        public string Description { get; set; }
        public int Size { get; set; }
        public Item(string description, int size)
        {
            this.Description = description;
            this.Size = size;
        }

        public override string ToString()
        {
            return Description;
        }
    }
}
