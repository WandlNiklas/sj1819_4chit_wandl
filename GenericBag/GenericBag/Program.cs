﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBag
{
    class Program
    {
        static void Main(string[] args)
        {
            //Bag<int> b = new Bag<int>(5);

            //b.putItem(2);
            //b.putItem(4);
            //b.putItem(6);
            //b.putItem(8);
            //b.putItem(10);
            //b.putItem(12);

            //Console.WriteLine(b.Content());

            //for (int i = 0; i < 6; i++)
            //{
            //    b.getItem();
            //}

            //Console.WriteLine(b.Content());

            //Bag<string> bs = new Bag<string>(3);

            //bs.putItem("Hey");
            //bs.putItem("Dude");
            //bs.putItem("Nice");
            //bs.putItem("David");

            //Console.WriteLine(bs.Content());

            //for (int i = 0; i < 2; i++)
            //{
            //    bs.getItem();
            //}

            //Console.WriteLine(bs.Content());

            Item d1 = new Item("2", 2);
            Item d2 = new Item("d", 1);
            Item d3 = new Item("2", 2);

            Bag<IDescription> b = new Bag<IDescription>(3);
            b.putItem(d1);
            b.putItem(d2);
            b.putItem(d3);

            Console.WriteLine(b.Content());

            b.getItem();

            Console.WriteLine(b.Content());
        }
    }
}
