﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericBag
{
    interface IDescription
    {
        string Description { get; set; }
        int Size { get; set; }
    }
}
